#!/usr/bin/python
# -*- coding: UTF-8  -*-
from PyQt4 import QtCore, QtGui, Qt
from mvc.model import ModelReport
from mvc.view import ViewReport
from mvc.control import ControlReport
import time 
from dbunit.dbunit import Bus, VedomBusViezd, DocTableServiceTo1To2, DocumentServiceTo1To2 
from dbunit.dbunit import ServiceTo1To2,  DocTableReadyServiceTo1To2
from sqlalchemy import func
from mvc.reports import PrintQt
from datetime import datetime


class DialogFindBus(QtGui.QDialog):
    'Быстрый поиск автобуса по гаражному'
    def __init__(self, parent = None):
        'Переопределяем для снятия выделения и инициализации переменных'
        QtGui.QDialog.__init__(self, parent)
        self.ed = QtGui.QLineEdit()
        self.dbutton = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|QtGui.QDialogButtonBox.Cancel)
        self.connect(self.dbutton, QtCore.SIGNAL("rejected()"),self.reject) 
        self.connect(self.dbutton, QtCore.SIGNAL("accepted()"),self.accept)
        self.timer = QtCore.QTimer()
        self.connect(self.timer, QtCore.SIGNAL("timeout()"), self.deselect)
        gr = QtGui.QGridLayout()
        gr.addWidget(self.ed)
        gr.addWidget(self.dbutton)
        self.setLayout(gr)
    
    def show(self, *args, **kwargs):
        'Переопределяем для старта таймера, чтобы снять выделение'
        self.timer.start(10)
        return QtGui.QDialog.show(self, *args, **kwargs)
    
    def deselect(self):
        'Стоп таймера снимаем выделение'
        self.timer.stop()
        self.ed.deselect()
#===============================================================================
# 
#===============================================================================

class ModelMehanik(ModelReport):
    '''Основная модель окна отчета механик
    
    * columnview = 6 - количество колонок в таблице
    * flags - флаги:
        * 1 - показывать 1-ю смену
        * 2 - показывать 2-ю смену
        * 3 - предпросмотр вместо печати'''
    columnview = 7
    colcol = 4
    data = []
    dataview = []
    flags = [True, False, False] #флаги 
    
    def header(self):
        head = []
        self.colcol = (self.flags[0] + self.flags[1]) * 2 
        for i in range(self.columnview):
            head += [u'Гар']
            if self.flags[0]:
                head += [u'1см\nвыезд\nплан\n(факт)']
                head += [u'1см\nвозвр\nплан\n(факт)']
            if self.flags[1]:
                head += [u'2см\nвыезд\nплан\n(факт)']
                head += [u'2см\nвозвр\nплан\n(факт)']
        return head
    
    def gethour(self, hour):
        'По полученному времени возвращаем строку'
        h = ''
        if hour:
            h = datetime.strftime(hour, "%H:%M")
        return h 
    
    def cleardataview(self, rowmax):
        'Подготавливаем данные для отображения'
        self.dataview = []
        #создаем пустую сетку
        for rows in range(rowmax):
            row = []
            for cols in range(self.columnview):
                col = ['']
                for data in range(self.colcol+1):#гаражный и 8 таймеров
                    col += ['']
                row += col
            self.dataview += [row]
    
    def getstrviezd(self, time1, time2):
        if self.gethour(time2):
            st = "%s(%s)"%(self.gethour(time1),self.gethour(time2))
        else:  
            st = "%s"%(self.gethour(time1))
        return st
    
    def datatodataview(self):
        'Преобразоваываем данные в представление'
        self.colcol = (self.flags[0] + self.flags[1]) * 2
        rowmax = len(self.data)/self.columnview + 1
        self.cleardataview(rowmax)
        crow = 0
        ccol = 0
        col = 0
        for rec in self.data:
            if crow > rowmax - 1:
                ccol += 1
                crow = 0
                col = ccol * (self.colcol + 1)
            #в зависимости от флагов отображаем данные
            inccol = 0    
            self.dataview[crow][col] = rec[0] #гаражный
            if self.flags[0]:
                inccol += 1
                stvd1 = self.getstrviezd(rec[2], rec[3])
                self.dataview[crow][col + inccol] = stvd1
                inccol += 1
                stvr1 = self.getstrviezd(rec[4], rec[5])
                self.dataview[crow][col + inccol] = stvr1
            if self.flags[1]:                            
                inccol += 1
                stvd2 = self.getstrviezd(rec[6], rec[7])
                self.dataview[crow][col + inccol] = stvd2
                inccol += 1
                stvr2 = self.getstrviezd(rec[8], rec[9])
                self.dataview[crow][col + inccol] = stvr2
            crow += 1
    
    def readrecords(self):
        '''Чтение данных - создание таблицы данных.
        
        Считываем автобусы, по свойству columnview определяем на сколько колонок разбить
        и заполняем таблицу данными гаражный, время выезда, время возвращения
        typedt принимаем, что типы идут в следующем порядке:
         
            * 1 - выезд 1 см план
            * 2 - выезд 1 см факт
            * 3 - возвращение 1 см план
            * 4 - возвращение 1 см факт
            * 5 - выезд 2 см план
            * 6 - выезд 2 см факт
            * 7 - возвращение 2 см план
            * 8 - возвращение 2 см факт
        
        в data заносим в следуюещем порядке (по колонкам):
            * гаражный для сортировки
            * запись с данными по автобусу (запись таблицы bus)
            * следущие 8 колонки согласно typedt 
        '''
        self.data = []
        bus = {}
        query = self.session.query(Bus).all()
        for row in query:
            tmp = [row.garnm, row]
            for i in range(8):
                tmp += [None]
            bus[row.id] = tmp
        query =  self.session.query(VedomBusViezd)
        for row in query:
            try:
                bus[row.busid][row.typedt+1] = row.dt #typedt тип времени
            except KeyError:
                print "in VedomBusViezd Not found bus with key",row.busid
        for key in bus.keys():
            self.data += [bus[key]]
        self.data = sorted(self.data) #сортируем по гаражному
        self.datatodataview()
        self.notifyobserver()
    
    def getbus(self, garnm):
        'По гаражному номеру получаем id'
        return self.session.query(Bus).filter(Bus.garnm == garnm).first()
    
    def setview(self, flag, state):
        '''Установка флагов, как хотим видеть данные'''
        self.flags[flag] = state
        self.datatodataview()
        self.notifyobserver()
#===============================================================================
# 
#===============================================================================
class ViewMehanik(ViewReport):
    'Вид отчета механик'
    windowtitle = u'Механик'
    
    def initwidget(self):
        'Определяем виджеты'
        self.timer = QtCore.QTimer()
        self.journal = QtGui.QTableWidget()
        self.journal.setEditTriggers(QtGui.QAbstractItemView.EditTriggers(0))
        self.clock = QtGui.QLabel(u'00:00:00')
        f = QtGui.QFont()
        f.setPointSize(40)
        self.clock.setFont(f)
        self.dialog = DialogFindBus()
        self.dialog.setModal(True)
        self.chsm1 = QtGui.QCheckBox(u'1 смена')
        self.chsm1.setCheckState(2)
        self.chsm2 = QtGui.QCheckBox(u'2 смена')
        self.chsm2.setCheckState(0)
        self.chpreview = QtGui.QCheckBox(u'Предпросмотр')
        self.chpreview.setCheckState(0)
    
    def initlayout(self):
        'Определяем слои и расположения виджеты'
        v = QtGui.QVBoxLayout()
        v.addWidget(self.chsm1)
        v.addWidget(self.chsm2)
        h = QtGui.QHBoxLayout()
        h.addWidget(self.clock)
        h.addLayout(v)
        h.addWidget(self.chpreview)
        h.addStretch(1)
        gr = QtGui.QGridLayout()
        gr.addLayout(h,0,0)
        gr.addWidget(self.journal)
        self.setLayout(gr)
    
    def setheader(self, header):
        'Определяем заголовки'
        self.journal.setColumnCount(len(header))
        self.journal.setHorizontalHeaderLabels(header)
        self.journal.resizeColumnsToContents()
        
    
    def settime(self, hour, minute, second):
        'Установить время (текущее)'
        self.clock.setText('%02d:%02d:%02d'%(hour, minute, second))
    
    def keyReleaseEvent (self, event):
        'Отлавливаем клавиши'
        if event.key() > 47 and event.key()<58:
            self.dialog.ed.setText(chr(event.key()))
            self.dialog.show()
        ViewReport.keyReleaseEvent(self, event)

    def redraw(self, data):
        'Перерисовываем таблицу'
        self.currowcol = (self.journal.currentRow(), self.journal.currentColumn())
        self.journal.setRowCount(0)
        self.journal.setRowCount(len(data))
        if len(data)>0:
            #self.journal.setColumnCount(len(data[0]))
            for row, rowdata in enumerate(data):
                for col, item in enumerate(rowdata):
                    self.itemtable(row, col, item)
            self.journal.resizeColumnsToContents()
            self.journal.resizeRowsToContents()
            self.journal.setCurrentCell(self.currowcol[0], self.currowcol[1])

    def itemtable(self,row,col,s):
        'Перерисовка ящейки'
        item = QtGui.QTableWidgetItem("%s"%s)
        self.journal.setItem(row, col, item)

#===============================================================================
# 
#===============================================================================
class ControlMehanik(ControlReport):
    'Контрол отчета механик'
    
    def __init__(self, parent = None):
        'Переопределяем для старта таймеров'
        model = ModelMehanik()
        model.addobserver(self)
        view = ViewMehanik() 
        view.setheader(model.header())
        view.dialog.connect(view.dialog, QtCore.SIGNAL('accepted()'), self.selected)
        ControlReport.__init__(self, model, view, parent)
        self.timeout()
        self.initaction()
        self.model.readrecords()
    
    def initaction(self):
        'Определяем действия, в частности работу таймера'
        self.view.connect(self.view.timer, QtCore.SIGNAL("timeout()"), self.timeout)
        self.view.connect(self.view, QtCore.SIGNAL("closewidget()"), self.stoptimer)
        self.view.connect(self.view.chsm1, QtCore.SIGNAL("stateChanged(int)"), self.setchecksm1)
        self.view.connect(self.view.chsm2, QtCore.SIGNAL("stateChanged(int)"), self.setchecksm2)
        self.view.connect(self.view.chpreview, QtCore.SIGNAL("stateChanged(int)"), self.setpreview)
        self.view.timer.start(1000)
    
    def setchecksm1(self, state):
        if state == 0 and self.view.chsm2.checkState() == 0:
            self.view.chsm1.setCheckState(2)
        else:
            self.model.setview(0, state/2)
    def setchecksm2(self, state):
        if state == 0 and self.view.chsm1.checkState() == 0:
            self.view.chsm1.setCheckState(2)
        self.model.setview(1, state/2)
    def setpreview(self, state):
        self.model.setview(2, state/2)
    
    def timeout(self):
        'Сработка таймера - устанавливаем текущее время'
        tm = time.localtime()
        self.view.settime(tm[3], tm[4], tm[5])
    
    def stoptimer(self):
        self.view.timer.stop()
    
    def modelischanged(self):
        'Обработка изменений в модели - паттерн наблюдатель'
        self.view.setheader(self.model.header())
        self.view.redraw(self.model.dataview)
    
    def selected(self):
        'Обработка выбора - вызываем диалоговое окно с информацией по автобусу'
        garnm = int(self.view.dialog.ed.text())
        
        bus = self.model.getbus(int(garnm))
        if bus:
            self.buspanel = ControlBus(bus.id) 
            self.view.dialogbus = QtGui.QDialog()
            widg = self.buspanel.widget()
            gr  = QtGui.QGridLayout()
            gr.addWidget(widg)
            self.view.dialogbus.setLayout(gr)
            self.view.dialogbus.setWindowTitle(self.buspanel.view.windowtitle)
            self.view.dialogbus.setModal(True)
            self.view.connect(widg, QtCore.SIGNAL("closewidget()"), self.closecontrol)
            self.view.dialogbus.show()
            
    def closecontrol(self):
        'Закрываем контрол'
        self.view.dialogbus.close()
            

#=======================================================================
# 
#=======================================================================
class ModelBus(ModelReport):
    '''Модель окна информации по автобусу
        
        data (dict):
            * bus(:class:`dbunit.dbunit.Bus`) - запись выбранного автобуса
            * to1(dict):
                * ready(datetime) - дата время прохождения ТО-1;
                * km(int)         - пробег на котором пройдено ТО-1;
                * plan(datetime)  - дата и время, когда запланировано ТО-1
            * to2(dict):
                * ready(datetime) - дата время прохождения ТО-2;
                * km(int)         - пробег на котором пройдено ТО-2;
                * plan(datetime)  - дата и время, когда запланировано ТО-2
            * to(dict):
                * ready(datetime) - дата время прохождения тех осмотра
                * plan(datetime)  - дата и время, когда запланировано тех осмотр
            * strah(dict):
                * ready(datetime) - дата время заключения страхового договора 
                * plan(datetime)  - дата и время, когда запланировано заключение страхового договора
            * service(dict):
                * to1(int)        - пробег для назначения ТО-1 
                * to2(int)        - пробег для назначения ТО-2
            * shkiper(dict):
                * ready(str)      - исправен или нет  
    
        dataview (dict):
            * bus(dict):
                * garnm(str)      - гаражный автобуса
                * gosnm(str)      - госномер автобуса
                * marka(str)      - марка автобуса
            * to1(dict):
                * ready(str(datetime "%d.%m.%Y)) - дата прохождения ТО-1;
                * km(str)         - пробег на котором пройдено ТО-1;
                * plan(str(datetime "%d.%m.%Y))  - дата и время, когда запланировано ТО-1
            * to2(dict):
                * ready(str(datetime "%d.%m.%Y)) - дата время прохождения ТО-2;
                * km(str)         - пробег на котором пройдено ТО-2;
                * plan(str(datetime "%d.%m.%Y))  - дата и время, когда запланировано ТО-2
            * to(dict):
                * ready(str(datetime "%d.%m.%Y)) - дата время прохождения тех осмотра
                * plan(str(datetime "%d.%m.%Y))  - дата и время, когда запланировано тех осмотр
            * strah(dict):
                * ready(str(datetime "%d.%m.%Y)) - дата время заключения страхового договора 
                * plan(str(datetime "%d.%m.%Y))  - дата и время, когда запланировано заключение страхового договора
            * service(dict):
                * to1(str)        - пробег для назначения ТО-1 
                * to2(str)        - пробег для назначения ТО-2
            * shkiper(dict):
                * ready(str)      - исправен или нет
            '''
        
    data = {}
    dataview = {}
    
    def readbus(self, busid):
        'Получаем информацию по автобусу'
        self.data["bus"] = self.session.query(Bus).filter(Bus.id == busid).first()
    
    def readserviceto1to2(self, busid):
        'Пробеги на прохождение ТО-1 ТО-2 для марки данного автобуса'
        self.data["service"] = {}
        to1km = 0
        to2km = 0
        serv = self.session.query(ServiceTo1To2).filter(ServiceTo1To2.markaid == self.data["bus"].markaid).first()
        if serv:
            to1km = serv.to1km
            to2km = serv.to2km
        self.data["service"]["to1"] = to1km
        self.data["service"]["to2"] = to2km
    
    def readplanservice(self, busid, typeserviceid = 1):
        '''Ищем дату запланированного прохождение обслуживания. 
        Возвращаем дату запланированного мероприятия либо None         '''
        #TODO: включить проверку, что меропрятие выполенно
        #находим максимальную дату когда планируется ТО-1 (ТО-2)
        dt = None
        classfind = DocTableServiceTo1To2
        dtplan = self.session.query(func.max(classfind.dt)).\
                                filter(classfind.typeserviceid == typeserviceid,
                                       classfind.busid == busid).\
                                group_by(classfind.busid).first()
        if dtplan:#если нашли что нибудь - проверяем, больше ли она даты сделанной
            dt = dtplan[0]
        return dt
        
    def readreadyservice(self, busid, typeserviceid = 1):
        '''Ищем дату прохождения сервисного обслуживания (ТО-1, ТО-2)'''
        dt = None
        classfind = DocTableReadyServiceTo1To2
        dtready = self.session.query(func.max(classfind.dt)).\
                                filter(classfind.typeserviceid == typeserviceid,
                                       classfind.busid == busid).\
                                group_by(classfind.busid).first()
        if dtready:
            dt = dtready[0]
        return dt
        
    def readprobeg(self, busid, dtready, typeserviceid = 1):
        'Ищем пробег пройденного обслуживания'
        km = 0
        classfind = DocTableReadyServiceTo1To2
        kmto12 = self.session.query(classfind).\
                    filter(classfind.typeserviceid == typeserviceid,
                           classfind.busid == busid,
                           classfind.dt == dtready).first()
        if kmto12:
            km = kmto12.km
        return km 
    
    def datetimetostr(self, dt):
        'Преобразуем дату в строку по формату %d.%m.%Y'
        return dt.strftime("%d.%m.%Y")
    
    def datatodataview(self):
        'Данные из data приводим к виду dataview'
        self.dataview = {}
        self.dataview["bus"] = {}
        self.dataview["bus"]["garnm"] = str(self.data["bus"].garnm)
        self.dataview["bus"]["gosnm"] = self.data["bus"].gosnm
        self.dataview["bus"]["marka"] = ''
        if self.data["bus"].marka:
            self.dataview["bus"]["marka"] = self.data["bus"].marka.name
        
        for i in [1,2]:
            dind = "to%d"%i
            self.dataview[dind] = {}
            
            dtready = self.data[dind]["ready"]
            self.dataview[dind]["ready"] = u'Нет данных о прохождении'
            self.dataview[dind]["km"] = u'Нет данных о пробеге'
            if dtready:
                self.dataview[dind]["ready"] = u"Пройдено: {0}".format(self.datetimetostr(dtready))
                self.dataview[dind]["km"] = u"{0} км".format(self.data[dind]["km"])
            dtplan = self.data[dind]["plan"]
            self.dataview[dind]["plan"] = u'' #не запланировано
            if dtplan:
                self.dataview[dind]["plan"] = u'Запланировано: {0}'.format(self.datetimetostr(dtplan))
        self.dataview["to"] = {}
        self.dataview["to"]["ready"] = u'Нет данных'
        self.dataview["to"]["plan"] = u''
        self.dataview["strah"] = {}
        self.dataview["strah"]["ready"] = u'Нет данных'
        self.dataview["strah"]["plan"] = u''
        self.dataview["shkiper"] = {}
        self.dataview["shkiper"]["ready"] = u'Нет данных'
        
    def readdata(self, busid):
        'Читаем все данные '
        self.data = {}
        self.readbus(busid)
        self.readserviceto1to2(busid)
        #читаем данные по ТО-1 и ТО-2
        for i in [1,2]:
            dind = "to%d"%i
            self.data[dind] = {}
            dtready = self.readreadyservice(busid, i)
            self.data[dind]["ready"] = dtready 
            self.data[dind]["km"] = 0
            if dtready:
                self.data[dind]["km"] = self.readprobeg(busid, dtready, i)
            self.data[dind]["plan"] = self.readplanservice(busid, i)
        self.datatodataview()
        self.notifyobserver()
#===============================================================================
# 
#===============================================================================
class TableWidgetKey(QtGui.QTableWidget):
    'QTableWidget переопределяем keyPressEvent - ловим нажатие ентеров'
    def keyPressEvent(self, event):
        'Переопределяем реакцию на клавиши для ловли ентеров'
        if event.key() == QtCore.Qt.Key_Return:
            self.emit(QtCore.SIGNAL("printing()"))
        if event.key() == QtCore.Qt.Key_Enter:
            self.emit(QtCore.SIGNAL("printing()"))
        QtGui.QTableWidget.keyPressEvent(self,event)
        
class ViewBus(ViewReport):
    'Вид окна информации по автобусу'
    windowtitle = u'Данные по автобусу'
    def initwidget(self):
        'Определяем виджеты'
        self.bus = QtGui.QLabel(u'123')
        self.to1 = QtGui.QLabel(u'0 (0)')
        self.to2 = QtGui.QLabel(u'0 (0)')
        self.to = QtGui.QLabel(u'Технический осмотр')
        self.strah = QtGui.QLabel(u'123')
        self.shkiper = QtGui.QLabel(u'Шкипер-01Е')
        self.tableprint = TableWidgetKey()
        self.tableprint.setEditTriggers(QtGui.QAbstractItemView.EditTriggers(0))
        
    def initlayout(self):
        'Определяем слои, расположение виджеты'
        gr = QtGui.QGridLayout()
        gr.addWidget(QtGui.QLabel(u'Автобус'), 0, 0)
        gr.addWidget(self.bus, 0, 1)
        gr.addWidget(QtGui.QLabel(u'ТО-1'), 1, 0)
        gr.addWidget(self.to1, 1, 1)
        gr.addWidget(QtGui.QLabel(u'ТО-2'), 2, 0)
        gr.addWidget(self.to2, 2, 1)
        gr.addWidget(QtGui.QLabel(u'Тех.осмотр'), 3, 0)
        gr.addWidget(self.to, 3, 1)
        gr.addWidget(QtGui.QLabel(u'Страховое'), 4, 0)
        gr.addWidget(self.strah, 4, 1)
        gr.addWidget(QtGui.QLabel(u'Шкипер-01Е'), 5, 0)
        gr.addWidget(self.shkiper, 5, 1)
        gr.addWidget(QtGui.QLabel(u'ТО-1'), 1, 0)
        gr.setSpacing(6)
        
        hl = QtGui.QHBoxLayout()
        hl.addLayout(gr)
        hl.addWidget(self.tableprint)
        self.setLayout(hl)
        self.setMinimumSize(900, 350)
    
    def redraw(self, dataview):
        'Перерисовка'
        str = u"<h1>Гаражный {0} ({1}) [{2}]</h1>". format(dataview['bus']["garnm"],
                                        dataview['bus']["marka"],
                                        dataview['bus']["gosnm"])
        self.bus.setText(str)
        
        str = u"{0} ({1}) {2}".format(dataview["to1"]["ready"],
                                   dataview["to1"]["km"],
                                   dataview["to1"]["plan"])
        self.to1.setText(str)
        str = u"{0} ({1}) {2}".format(dataview["to2"]["ready"],
                                   dataview["to2"]["km"],
                                   dataview["to2"]["plan"])
        self.to2.setText(str)
        str = u"{0} {1}".format(dataview["to"]["ready"],
                                dataview["to"]["plan"])
        self.to.setText(str)    
        str = u"{0} {1}".format(dataview["strah"]["ready"],
                                dataview["strah"]["plan"])
        self.strah.setText(str)
        str = u"{0} ".format(dataview["shkiper"]["ready"])
        self.shkiper.setText(str)
        
        rows = [
            u'Убытие - Основное',
            u'Прибытие - Основное',
            u'Убытие - Заказ',
            u'Прибытие - Заказ',
            u'Убытие - Хозпарк',
            u'Прибытие - Хозпарк',
            u'Убытие - Международная',
            u'Прибытие - Международная']
        
        self.tableprint.setColumnCount(1)
        self.tableprint.setRowCount(len(rows))
        fnt = QtGui.QFont()
        fnt.setPixelSize(30)
        for ind, row in enumerate(rows):
            item = QtGui.QTableWidgetItem(row)
            item.setFont(fnt)
            self.tableprint.setItem(ind,0,item)
        self.tableprint.resizeColumnsToContents()
        self.tableprint.resizeRowsToContents()
        self.tableprint.setCurrentCell(0,0)
    
    def getrow(self):
        'Получаем текущую строку'
        return self.tableprint.currentRow()
            
#===========================================================================
# 
#===========================================================================
class ControlBus(ControlReport):
    'Контрол информации по автобусу'
    def __init__(self, busid, parent=None):
        'Переопределяем создание'
        model = ModelBus()
        view = ViewBus()
        ControlReport.__init__(self, model, view, parent=parent)
        model.readdata(busid)
        self.signals()

    def modelischanged(self):
        'Реагирование на изменение модели'
        self.view.redraw(self.model.dataview) 
    
    def signals(self):
        'Реакции виджета'
        self.view.connect(self.view.tableprint, QtCore.SIGNAL("cellClicked(int ,int )"), self.sendprint)
        self.view.connect(self.view.tableprint, QtCore.SIGNAL("printing()"), self.sendprint)
    
    def sendprint(self, row = -1, col = -1):
        'Отправка на печать'
        row = self.view.getrow()
        dviezd = ''
        dvozvr = ''
        if row == 0 or row == 2:
            dviezd = datetime.now().strftime("%m.%d %H:%M")
        else:
            dvozvr = datetime.now().strftime("%m.%d %H:%M")
        if row == 0 or row == 1:
            templ = 'template/mehhoz.svg'
        else:
            templ = 'template/mehput.svg'
        data = {}
        data["timeviezd"] = dviezd
        data["timevozvr"] = dvozvr
        self.view.pr = PrintQt()
        self.view.pr.setsettings(3, True)
        self.view.pr.data = data
        self.view.pr.filetemplate = templ  
        #self.view.pr.printpreview()
        self.view.pr.printing()
        self.view.close()
    
