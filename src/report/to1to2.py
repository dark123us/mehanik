#!/usr/bin/python
# -*- coding: utf-8  -*-
from mvc.model import Report as MReport 
from mvc.view import Report as VReport
from mvc.control import Report as CReport


from dbunit.dbunit import Bus, Marka, Probeg,  ServiceTo1To2

class MReportTo1To1Select(MReport):
    def getheader(self):
        pass
    
    def gettypecolumns(self):
        pass
    
    def getforview(self):
        pass
    
    def getquery(self):
        return self.session.query(ServiceTo1To2).join(Marka).order_by(Marka.name)

class VReportTo1To1Select (VReport):
    windowtitle = u'Подбор на ТО-1 ТО-2'
    def initwidget(self):
        pass
    def initlayout(self):
        pass

class CReportTo1To1Select(CReport):
    ''

class To1To2(CReportTo1To1Select):
    ''
    def __init__(self, parent=None):
        model = MReportTo1To1Select()
        view = VReportTo1To1Select()
        CReportTo1To1Select.__init__(self, model, view, parent=parent)
        
    def setModal(self, modal):
        self.view.setModal(modal)
    
    def setMinimumSize(self, width, height):
        self.view.setMinimumSize(width, height)
    
    def connect(self, qtobject, signal, slot):
        self.view.connect(qtobject, signal, slot)
    
    def show(self):
        self.view.show()