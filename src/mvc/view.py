#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from datetime import datetime, date, time

class View(QtGui.QWidget):
    windowtitle = u'Вид'
    'абстрактный класс view компонента'
    def __init__(self, parent = None):
        QtGui.QWidget.__init__(self, parent)
        self.setWindowTitle(self.windowtitle)
        self.initwidget()
        self.initlayout()
    
    def initwidget(self):
        pass
    
    def initlayout(self):
        pass
    
    def closeEvent(self, event):
        self.emit(QtCore.SIGNAL("closewidget()"))
        QtGui.QWidget.closeEvent(self, event)

class ViewHandbook(View):
    'абстрактный класс view компонента справочника'
    panel = None        # QtGui.QPa #панель с кнопками
    table = None        # QtGui.QTableWidget() 
    dbutton = None      #кнопки диалога
    typecolumns = []    #типы колонок, handbook() str, int, real, date, time
    currowcol = [0, 0]  #при редактировании запоминаем ячейку из которой вызвали диалог
    windowtitle = u'Справочник'
        
    def initwidget(self):
        self.table = QtGui.QTableWidget()
        self.panel = QtGui.QToolBar()
        self.panel.setIconSize(QtCore.QSize(32,32))
        self.dbutton = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Cancel)
        grl = QtGui.QGridLayout()
        grl.addWidget(self.panel)
        grl.addWidget(self.table)
        grl.addWidget(self.dbutton)
        self.setLayout(grl)
        self.a_add = QtGui.QAction(QtGui.QIcon('img/add.png'), u'Добавить', self)
        self.a_add.setShortcut(QtCore.Qt.Key_Insert)
        self.a_rm = QtGui.QAction(QtGui.QIcon('img/rm.png'), u'Удалить', self)
        self.a_rm.setShortcut(QtCore.Qt.Key_Delete)
        self.panel.addAction(self.a_add)
        self.panel.addAction(self.a_rm)
    
    def redraw(self, data):
        self.currowcol = [self.table.currentRow(), self.table.currentColumn()] 
        self.table.setRowCount(0)
        self.table.setRowCount(len(data))
        for row, rowdata in enumerate(data):
            for col, item in enumerate(rowdata):
                if type(item) == int or type(item) == long:
                    s = "%d"%item
                elif type(item) == str or type(item) == unicode:
                    s = item
                else:
                    s = ''
                self.itemtable(row,col,s)
        self.table.resizeColumnsToContents()
        self.table.resizeRowsToContents()
        self.table.setCurrentCell(self.currowcol[0], self.currowcol[1])
    
    def itemtable(self,row,col,s):
        item = QtGui.QTableWidgetItem("%s"%s)
        self.table.setItem(row, col, item)
        
    def getrow(self):
        return self.table.currentRow()
    
    def setediting(self, column = 0):
        sel = False
        row = self.table.currentRow() 
        if column <= self.table.columnCount():
            if self.typecolumns[column] == int or self.typecolumns[column] == long:
                sel = True
                'edit int'
            elif self.typecolumns[column] == str or self.typecolumns[column] == unicode:
                sel = True
                'edit str'
            elif self.typecolumns[column] == datetime:
                sel = True
                'edit detetime'
            else:
                self.emit(QtCore.SIGNAL('getvaluedict(QVariant)'),self.typecolumns[column])
            if sel:
                self.table.setCurrentCell(row, column)
                self.table.editItem(self.table.item(row, column))
    
    def setheader(self, header):#установка имен заголовков
        self.table.setColumnCount(len(header))
        self.table.setHorizontalHeaderLabels(header)
        self.table.resizeColumnsToContents()

#===============================================================================
# 
#===============================================================================
class ViewReport(View):
    'Отчет'
    windowtitle = u'Отчет'
    
    def initwidget(self):
        raise Exception("Need initialization initwidget in Report")
    
    def initlayout(self):
        raise Exception("Need initialization initlayout in Report")
    
#===============================================================================
# 
#===============================================================================
class ViewJournal(View):
    windowtitle = u'Журнал'
    
    def initwidget(self):
        self.table = QtGui.QTableWidget()
        self.table.setEditTriggers(QtGui.QAbstractItemView.EditTriggers(0))
    
    def initlayout(self):
        gr = QtGui.QGridLayout()
        gr.addWidget(self.table)
        self.setLayout(gr)

    def setheader(self, header):#установка имен заголовков
        self.table.setColumnCount(len(header))
        self.table.setHorizontalHeaderLabels(header)
        self.table.resizeColumnsToContents()
    
    def redraw(self, data):
        self.currowcol = [self.table.currentRow(), self.table.currentColumn()] 
        self.table.setRowCount(0)
        self.table.setRowCount(len(data))
        for row, rowdata in enumerate(data):
            for col, item in enumerate(rowdata):
                if type(item) == int or type(item) == long:
                    s = "%d"%item
                elif type(item) == str or type(item) == unicode:
                    s = item
                elif type(item) == datetime:
                    s = item.strftime("%y-%m-%d %H:%M:%S")
                else:
                    s = ''
                self.itemtable(row,col,s)
        self.table.resizeColumnsToContents()
        self.table.resizeRowsToContents()
        self.table.setCurrentCell(self.currowcol[0], self.currowcol[1])
    
    def itemtable(self,row,col,s):
        item = QtGui.QTableWidgetItem("%s"%s)
        self.table.setItem(row, col, item)
    
    def keyReleaseEvent (self, event):
        #ViewJournal.keyReleaseEvent(self, event)
        if event.key() == 16777220 or  event.key() == 16777221:
            self.table.emit(QtCore.SIGNAL("pressed(int, int)"), self.table.currentRow(), self.table.currentColumn())
        
        