#!/usr/bin/python
# -*- coding: utf-8  -*-
from datetime import datetime, date
import re
from model import Model
from view import View
from control import Control
from dbunit.dbunit import SinglEngine
from sqlalchemy import func
from PyQt4 import QtGui, QtCore
 
class ModelDocument(Model):
    '''Шаблон модели документа. Поведение: создание нового, открытие документа.
    
    Свойства::
        * curdoc = None - текущая запись документа в ORM и доступ к данным его элементов 
        * currec = None - текущая запись строки таблицы документа в ORM
        * data = [] - список записей ORM таблицы документа
        * dataview = [] - двумерная таблица со значениями для таблицы документа для view
        * docname = 'документ' - название документа (используется журналом)
        * recompile = re.compile(r'([^0-9]*)([0-9]*)$') - разбор цифрового кода номера документа
        
    '''            
    author = 'Administrator'
    curdoc = None   #ссылка на самого себя
    currec = None
    data = []
    dataview = []
    docname = u'Документ'
    recompile = re.compile(r'^([^0-9]*)([0-9]*)$')

    def __init__(self, opendocid = 0):
        'При создании считываем данные по документу и его таблице или создаем новый документ'
        Model.__init__(self)  
        if opendocid == 0: #раз ид не прислали создаем новый документ
            self.newdoc()
        else: #иначе ищем документ
            self.opendoc(opendocid)
        
    #------------------------------------------------------------------
    #    Абстрактные методы
    #------------------------------------------------------------------
    def getclassdoc(self): 
        '''.. warning::
        
            Абстрактный метод, возвращаем класс отвечающий за основные поля документа, используем
            при создании нового документа.
        
        Пример::
        
            return DocumentServiceTo1To2()'''
        raise  Exception("Need initialization method getclassdoc in document model")
    
    def getclasstable(self, docid): 
        '''.. warning::
            
            Абстрактный метод, возвращаем объект с начальными данными
            отвечающий за табличные данные поля документа, используем
            при создании новых строк документа 
        
        Пример::
        
            return DocTableServiceTo1To2(docid)'''
        raise  Exception("Need initialization method getclasstable in document model")
    
    def getquerydoc(self, docid):
        '''.. warning::
        
            Абстрактный метод, возвращем запрос сессии для получения документа
        
        Пример::
        
            return self.session.query(DocumentServiceTo1To2)
                .filter(DocumentServiceTo1To2.id == docid).first()'''
        raise  Exception("Need initialization method getquerydoc in document model")
    
    def getquerytable(self, docid): 
        '''.. warning::
            
            Абстрактный метод, возвращаем запрос сессии для получения табличных записей документа
        
        Пример::
        
            return self.session.query(DocTableServiceTo1To2)
                    .join(Bus).join(TypeService)
                    .filter(DocTableServiceTo1To2.docid == id).all()
                    .order_by(Bus.garnm)'''
        raise  Exception("Need initialization method getquerytable in document model")
    
    def datatoview(self, record):
        '''.. warning::
        
            Абстрактный метод, возвращаем преобразование записи в виде строки для вида
        
        Пример::
        
            dt = record.dt.strftime("%d.%m.%Y")
            return [record.bus.name, dt, record.typeservice.name]'''
        raise  Exception("Need initialization method datatoview in model")
    
    def header(self):
        '''.. warning::
        
            Абстрактный метод возвращаем список имен заголовков таблицы
        
        Пример::
        
            return (u'Гаражный', u'Тип обслуживания', u'Дата проведения')''' 
        raise  Exception("Need initialization method header in model")
    
    def gettypecolumns(self):
        '''.. warning::
                
            Абстрактный метод. Типы колонок для определения, что делать при попытке изменить данную колонку
        
        Пример::
        
            return [int, str, Model]'''
        raise  Exception("Need initialization method gettypecolumns in model")
    
    def update(self, row, col, value):
        '''.. warning::
        
            Абстрактный метод. Определяем как обновить ячейку табличных данных
        
        Пример::
        
            if col == 0:
                if value > 0:
                    self.data[row].busid = value
                    self.session.commit()
                    name = self.data[row].bus.garnm
                    self.dataview[row][col] = name
            self.notifyobserver()'''
        raise  Exception("Need initialization method update in model")

    #---Абстрактные методы---------------------------------------------
    #------------------------------------------------------------------
    #    Методы утилиты
    #------------------------------------------------------------------
    def getmaxnumber(self):
        'Генерируем номер для нового документа - удаляем ведущие символы, цифровую часть увеличиваем на 1 '
        number = '0'*8
        query = self.session.query(func.max(type(self.getclassdoc()).docnumber))#.group_by(DocumentServiceTo1To2.id)
        for row in query:
            if row[0]:
                num = self.recompile.search(row[0]).groups()
                number = int(num[1]) + 1 
                nole = 8 - len(num[0]) - len(str(number))
                if nole<0: nole = 0 
                number = "{0}{1}{2}".format(num[0], '0'*nole, number)
        return number
    
    def datetostr(self, dt):
        'Преобразование даты в строку, по умолчанию формат "%d.%m.%Y"' 
        return dt.strftime("%d.%m.%Y")
    #---Методы утилиты---------------------------------------------
    
    # Открытие документа---------------------------------------------
    def newdoc(self):
        'Обработка создания нового документа'
        self.curdoc = self.getclassdoc()
        self.curdoc.docnumber = self.getmaxnumber()
        self.curdoc.docdate = datetime.now()
        self.curdoc.userid = 1 #FIXME необходимо верное значение автора
        self.curdoc.isheld = False
        self.session.add(self.curdoc)
        self.session.commit()
    
    def opendoc(self, docid):
        'Открытие документа'
        self.curdoc = self.getquerydoc(docid)
        if not self.curdoc:
            self.newdoc()
        else:
            self.readtable(docid)
    
    def readtable(self, docid):
        'Читаем данные по таблице'
        rows = self.getquerytable(docid)
        self.data = []
        self.dataview = []
        for rec in rows:
            self.data.append(rec)
            self.dataview.append(self.datatoview(rec))
        self.notifyobserver()
    
    def cleartable(self):
        'Очищаем таблицу с данными'
        for rec in self.data:
            self.session.delete(rec)
        self.data = []
        self.dataview = []
        self.session.commit()
        self.notifyobserver()
    
    # Редактирование документа ------------------------------------------------
    def addrecord(self):
        'Добавляем строку в таблицу'
        self.currec = self.getclasstable(self.curdoc.id)
        self.session.add(self.currec)
        self.session.commit()
        self.data += [self.currec]
        self.dataview += [self.datatoview(self.currec)]
        self.notifyobserver()

    def rmrecord(self, row):
        'Удаляем строку из таблицы'
        item = self.data[row]
        self.session.delete(item)
        self.session.commit()
        del self.data[row]
        del self.dataview[row]
        self.notifyobserver()
    
    def rejectadd(self):#отмена удаления - сбрасываем последний элемент
        'Отменяем добавление строки'
        self.data = self.data[:-1]
        self.dataview = self.dataview[:-1]
        self.session.delete(self.currec)
        self.session.commit()
        self.notifyobserver()
#===============================================================================
# 
#===============================================================================
class ViewDocument(View):
    '''Абстрактный класс вида документа (шаблон)
    
    Свойства::
        * windowtitle - заголовок окна
    '''
    windowtitle = u'Документ'
    
    def initwidget(self):
        'Отрисовка виджета, добавляем и настраиваем элементы виджета документа по умолчанию'
        self.docdata = QtGui.QDateEdit()
        self.docdata.setCalendarPopup(True)
        self.docdata.setDateTime(QtCore.QDateTime(datetime.now()))
        
        self.docnumber = QtGui.QLineEdit(u'1')
        
        self.table = QtGui.QTableWidget()
        self.table.setEditTriggers(QtGui.QAbstractItemView.EditTriggers(0))
        
        self.dbutton = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Save|QtGui.QDialogButtonBox.Cancel)
        
        
        self.a_add = QtGui.QAction(QtGui.QIcon('img/add.png'), u'Добавить', self)
        self.a_rm = QtGui.QAction(QtGui.QIcon('img/rm.png'), u'Удалить', self)

        self.panel = QtGui.QToolBar()
        self.panel.setIconSize(QtCore.QSize(32,32))
        self.panel.addAction(self.a_add)
        self.panel.addAction(self.a_rm)

    def initlayout(self):
        'Определяем как виджеты будут располагаться на основном слое grl - слой, который можно переопределить'
        self.grl = QtGui.QGridLayout()
        hl = QtGui.QHBoxLayout()
        hl.addWidget(QtGui.QLabel(u'Номер документа'))
        hl.addWidget(self.docnumber)
        hl.addWidget(QtGui.QLabel(u'от'))
        hl.addWidget(self.docdata)
        hl.addStretch(1)
        
        self.grl.addLayout(hl, 0, 0)
        self.grl.addWidget(self.panel)
        self.grl.addWidget(self.table)
        self.grl.addWidget(self.dbutton)
        self.setLayout(self.grl)

    def redraw(self, data, datatable):
        '''Перерисовываем данные на виджетах, data - элемнт ORM, отвечающий за 
        основные элементы документа, datatable - табличные данные '''
        self.redrawdata(data)   
        self.redrawtable(datatable)
    
    def redrawdata(self, data):
        'Перерисовываем основные данные, вызывается из :func:`redraw`'
        self.docdata.setDate(QtCore.QDate(data.docdate))
        self.docnumber.setText(data.docnumber)
    
    def redrawtable(self, data):
        'Перерисовываем табличные данные, вызывается из :func:`redraw`'
        self.currowcol = [self.table.currentRow(), self.table.currentColumn()] 
        self.table.setRowCount(0)
        self.table.setRowCount(len(data))
        for row, rowdata in enumerate(data):
            for col, item in enumerate(rowdata):
                if type(item) == int or type(item) == long:
                    s = "%d"%item
                elif type(item) == str or type(item) == unicode:
                    s = item
                else:
                    s = ''
                self.itemtable(row,col,s)
        self.table.resizeRowsToContents()
        self.table.resizeColumnsToContents()
        self.table.setCurrentCell(self.currowcol[0], self.currowcol[1])
    
    def itemtable(self,row,col,s):
        'Определяем как отрисовывается ячейка в таблице'
        item = QtGui.QTableWidgetItem("%s"%s)
        self.table.setItem(row, col, item)
    
    def setheader(self, header):#установка имен заголовков
        'Выводим заголовки таблицы'
        self.table.setColumnCount(len(header))
        self.table.setHorizontalHeaderLabels(header)
        self.table.resizeColumnsToContents()
    
    def getrow(self):
        'Вовращаем номер выделенной строки'
        return self.table.currentRow()
    
    def setediting(self, column = 0):
        'Перейти в режим редактирования в текущей строке, указанной колонке'
        self.table.editItem(self.table.item(self.table.currentRow(), column))
#===========================================================================
# 
#===========================================================================
class ControlDocument(Control):
    '''Контроллер документа (шаблон) '''
    def __init__(self, model, view, parent= None):
        Control.__init__(self, model, view, parent)
        view.setheader(self.model.header())
        model.notifyobserver()
        self.view.connect(self.view.table, QtCore.SIGNAL("cellChanged ( int, int )"), self.cellchanged)
        self.view.connect(self.view.table, QtCore.SIGNAL("cellDoubleClicked ( int, int )"), self.dubleclicked)
        self.view.connect(self.view.a_add, QtCore.SIGNAL('triggered()'), self.addrecord)
        self.view.connect(self.view.a_rm, QtCore.SIGNAL('triggered()'), self.rmrecord)

    def getvaluedict(self, value):
        '''.. warning::
            
            Абстрактный метод. Обработка события, что делать с вложенной ячейкой
        
        Пример::
        
            if value.toPyObject() == ModelMarka:
                self.selecthandbook(ModelMarka, ViewMarka, ControlMarka)
        '''
        raise Exception("Need initialization method getvaluedict in control")

    def cellchanged(self, row, col):
        'Данные в ячейке изменены (редактируем число или строку)'
        self.view.table.blockSignals(True)
        self.model.update(row, col, self.view.table.item(row, col).text())
        self.view.table.blockSignals(False)
    
    def selectdate(self, column):
        'При клике по ячейке типа дата, вызываем диалог выбора даты'
        self.view.dialog = QtGui.QDialog()
        gr = QtGui.QGridLayout()
        self.datedialog = QtGui.QDateEdit()
        self.datedialog.setCalendarPopup(True)
        self.datedialog.setDate(QtCore.QDate(date.today()))
        dbutton = QtGui.QDialogButtonBox()
        dbutton.addButton(QtGui.QDialogButtonBox.Ok)
        gr.addWidget(self.datedialog)
        gr.addWidget(dbutton)
        self.view.dialog.setLayout(gr)
        self.view.dialog.setWindowTitle(u"Выбор даты")
        self.view.dialog.connect(dbutton, QtCore.SIGNAL("accepted()"),self.view.dialog.accept)
        #dialog.connect(dialog, QtCore.SIGNAL("rejected()"),dialog.reject) 
        self.view.dialog.connect(self.view.dialog, QtCore.SIGNAL("accepted()"),self.dateaccept)
        self.view.dialog.setModal(True)
        self.view.dialog.show()
    
    def dubleclicked(self, row, col):
        'Обработка сигнала двойной клик по ячейке таблицы, для начала редактирования'
        self.view.table.setCurrentCell(row, col)
        typecolumn = self.model.gettypecolumns()[col]
        if typecolumn == int or typecolumn == long:
            self.view.setediting(col)
        elif typecolumn == str or typecolumn == unicode:
            self.view.setediting(col)
        elif typecolumn == datetime or typecolumn == date:
            self.selectdate(col)
        else:
            self.getvaluedict(typecolumn)
    
    def addrecord(self):#начинаем ввод новой строки
        'Обработка сигнала - добавление новой строки'
        self.view.table.blockSignals(True)
        self.model.addrecord()
        self.view.table.setCurrentCell(self.view.table.rowCount()-1, 0)
        typecolumn = self.model.gettypecolumns()[0]

        if typecolumn == int or typecolumn == long:
            self.view.setediting(0)
        elif typecolumn == str or typecolumn == unicode:
            self.view.setediting(0)
        elif typecolumn == datetime:
            self.view.setediting(0)
        else:
            self.getvaluedict(typecolumn)
        self.view.table.blockSignals(False)
    
    def rmrecord(self):
        'Удаление записи'
        if self.view.table.rowCount()>0:
            row =  self.view.table.currentRow()
            dlg = QtGui.QMessageBox(self.view)
            dlg.setWindowTitle(u'Удаление записи')
            dlg.setText(u'''Вы действительно хотите удалить %d-ю строку?'''%(row+1))
            dlg.setStandardButtons(QtGui.QMessageBox.Cancel | QtGui.QMessageBox.Ok );
            ok = dlg.exec_()
            if ok==QtGui.QMessageBox.Ok :
                self.model.rmrecord(row)

    def modelischanged(self):
        'Обработка сообщения от модели об ее изменении (паттерн наблюдатель)'
        self.view.table.blockSignals(True)
        self.view.redraw(self.model.dataview)
        self.view.table.blockSignals(False)
    
    def setselectmode(self, mode):
        'Установка режима для выбора '
        #self.selectmode = mode
        if mode:
            self.dialog = QtGui.QDialog()
            gr = QtGui.QGridLayout()
            gr.addWidget(self.view)
            self.dialog.setLayout(gr)
            self.dialog.setWindowTitle(self.dialogname)
            self.dialog.setMinimumSize(500,500)
            self.view.dbutton.addButton(QtGui.QDialogButtonBox.Ok)
            self.dialog.connect(self.view.dbutton, QtCore.SIGNAL("rejected()"),self.dialog.reject) 
            self.dialog.connect(self.view.dbutton, QtCore.SIGNAL("accepted()"),self.dialog.accept)
    
    def selecthandbook(self, model, view, control):
        'Выбор справочника'
        self.modelhandbook = model()
        self.viewhandbook = view()
        self.controlhandbook = control(self.modelhandbook, self.viewhandbook)
        self.controlhandbook.setselectmode(True)
        self.controlhandbook.dialog.connect(self.controlhandbook.dialog, 
                                         QtCore.SIGNAL("accepted()"), 
                                         self.accepthandbook)
#        self.controlhandbook.dialog.connect(self.controlhandbook.dialog, 
#                                         QtCore.SIGNAL("rejected()"), 
#                                         self.rejectadd)
        self.controlhandbook.dialog.setModal(True)
        self.controlhandbook.dialog.show()
    
    def rejectadd(self):#в случае отмены выбора элемента сбрасываем всю добавленную строку
        'Удаляем строку, которую ошибочно начали добавлять'
        self.model.rejectadd()
    #TODO: необходимо переработка методов accepthandbook dateaccept - одинаковый код 
    def accepthandbook(self):
        'Обработка сигнала accepted для выбора из справочника'
        self.view.table.blockSignals(True)
        row = self.viewhandbook.table.currentRow()      #выбранная строка, чтобы получить id
        column = self.view.table.currentColumn()#колонка, которую редактируем
        valueid = self.modelhandbook.data[row].id    #id которое выбрали
        rowupdated = self.view.table.currentRow()       #в какой строке модели необходимо провести изменения
        self.model.update(rowupdated, column, valueid)  #обновляем в модели значение
        #self.view.setediting(column + 1)
        self.view.table.blockSignals(False)
    
    def dateaccept(self):
        'Обработка сигнала accepted для даты'
        self.view.table.blockSignals(True)
        value = self.datedialog.date().toPyDate()    #id которое выбрали
        column = self.view.table.currentColumn()#колонка, которую редактируем
        rowupdated = self.view.table.currentRow()       #в какой строке модели необходимо провести изменения
        self.model.update(rowupdated, column, value)  #обновляем в модели значение
        #self.view.setediting(column + 1)
        self.view.table.blockSignals(False)