#!/usr/bin/python
# -*- coding: utf-8  -*-
#from model import HMServiceTo1To2, HMMarka, DMServiceTo1To2
#from view import HVServiceTo1To2, HVMarka, DVServiceTo1To2
from dbunit.dbunit import SinglEngine
from PyQt4 import QtCore, QtGui
from datetime import datetime, date

class Control():
    'control компонент MVC паттерна'
    model = None
    view = None
    
    def __init__(self, model, view, parent = None):
        self.view = view
        self.view.connect(self.view, QtCore.SIGNAL('closewidget()'), self.close)
        self.model = model
        self.model.addobserver(self)
    
    def widget(self):
        'Возвращаем виджет'
        return self.view
 
    def show(self):
        'Отображает виджет'
        return self.view.show()    
    
    def close(self):
        'При закрытии виджета отписываемся от модели'
        self.model.removeobserver(self)
#===============================================================================
# 
#===============================================================================
class ControlHandbook(Control):
    'справочник'
    dialogname = u'Справочник'  #Имя заголовка диалога
    def __init__(self, model, view, parent= None):
        Control.__init__(self, model, view, parent)
        view.setheader(self.model.header())
        self.view.typecolumns = self.model.gettypecolumns()
        self.model.readrecords()
        self.view.connect(self.view.table, QtCore.SIGNAL("cellChanged ( int, int )"), self.cellchanged)
        self.view.connect(self.view.table, QtCore.SIGNAL("cellDoubleClicked ( int, int )"), self.dubleclicked)
        self.view.connect(self.view.a_add, QtCore.SIGNAL('triggered()'), self.addrecord)
        self.view.connect(self.view.a_rm, QtCore.SIGNAL('triggered()'), self.rmrecord)
        self.view.connect(self.view, QtCore.SIGNAL('getvaluedict(QVariant)'), self.getvaluedict)

    def getvaluedict(self):#переопределить, что делать с вложенной ячейкой
        raise Exception("Need initialization method getvaluedict in control")

    def cellchanged(self, row, col):
        self.model.update(row, col, self.view.table.item(row, col).text())
    
    def dubleclicked(self, row, col):
        #FIXME забрать управление у вида - о типе колонки сообщить модели и виду сказать, в каком виде редактировать
        self.view.setediting(col)
    
    def addrecord(self):#начинаем ввод новой строки
        self.model.addrecord()
        self.view.table.setCurrentCell(self.view.table.rowCount()-1, 0)
        self.view.setediting()
    
    def rmrecord(self):
        if self.view.table.rowCount()>0:
            row =  self.view.table.currentRow()
            dlg = QtGui.QMessageBox(self.view)
            dlg.setWindowTitle(u'Удаление записи')
            dlg.setText(u'''Вы действительно хотите удалить %d-ю строку?'''%(row+1))
            dlg.setStandardButtons(QtGui.QMessageBox.Cancel | QtGui.QMessageBox.Ok );
            ok = dlg.exec_()
            if ok==QtGui.QMessageBox.Ok :
                self.model.rmrecord(row)

    def modelischanged(self):
        self.view.table.blockSignals(True)
        self.view.redraw(self.model.dataview)
        self.view.table.blockSignals(False)
    
    def setselectmode(self, mode):
        #self.selectmode = mode
        if mode:
            self.dialog = QtGui.QDialog()
            gr = QtGui.QGridLayout()
            gr.addWidget(self.view)
            self.dialog.setLayout(gr)
            self.dialog.setWindowTitle(self.dialogname)
            self.dialog.setMinimumSize(500,500)
            self.view.dbutton.addButton(QtGui.QDialogButtonBox.Ok)
            self.dialog.connect(self.view.dbutton, QtCore.SIGNAL("rejected()"),self.dialog.reject) 
            self.dialog.connect(self.view.dbutton, QtCore.SIGNAL("accepted()"),self.dialog.accept)
    
    def selecthandbook(self, model, view, control):
        self.modelhandbook = model()
        self.viewhandbook = view()
        self.controlhandbook = control(self.modelhandbook, self.viewhandbook)
        self.controlhandbook.setselectmode(True)
        self.controlhandbook.dialog.connect(self.controlhandbook.dialog, 
                                         QtCore.SIGNAL("accepted()"), 
                                         self.accepthandbook)
        self.controlhandbook.dialog.connect(self.controlhandbook.dialog, 
                                         QtCore.SIGNAL("rejected()"), 
                                         self.rejectadd)
        self.controlhandbook.dialog.setModal(True)
        self.controlhandbook.dialog.show()
    
    def rejectadd(self):#в случае отмены выбора элемента сбрасываем всю добавленную строку
        self.model.rejectadd()
    
    def accepthandbook(self):
        row = self.viewhandbook.table.currentRow() #выбранная строка в справочнике, чтобы получить id
        column = self.view.table.currentColumn()   #колонка, которую редактируем
        valueid = self.modelhandbook.data[row].id    #id которое выбрали
        rowupdated = self.view.table.currentRow()       #в какой строке модели необходимо провести изменения
        self.model.update(rowupdated, column, valueid)  #обновляем в модели значение
        self.view.setediting(column + 1)
    
#===========================================================================
# Документы
#===========================================================================


#=======================================================================
#  Обработка отчетов 
#=======================================================================
class ControlReport(Control):
    'Контрол отчета'
    def __init__(self, model, view, parent= None):
        Control.__init__(self, model, view, parent)
        #model.notifyobserver()

#===============================================================================
# 
#===============================================================================
class ControlJournal(Control):
    'Контрол журнала'
    def __init__(self, model, view, parent= None):
        Control.__init__(self, model, view, parent)
        model.notifyobserver()

    
        
    
