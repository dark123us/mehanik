#!/usr/bin/python
# -*- coding: UTF-8  -*-
"""
.. module:: model
   :platform: Unix, Windows
   :synopsis: A useful module indeed.

.. moduleauthor:: Prigodich Vadim <7676562@gmail.com>


"""
from dbunit.dbunit import SinglEngine

class Observer(object):#паттерн наблюдатель
    """Паттерн наблюдатель
    
    Используется как основа для MVC паттерна
    """
    observers = []  #список наблюдателей
    def __init__(self):
        self.observers = []    
    def addobserver(self, inobserver):
        '''Добавление наблюдателя'''
        self.observers.append(inobserver)
    def notifyobserver(self):
        '''Извещение наблюдателей, налюдатели должны реализовать метод :func:`modelischanged`'''
        for i in self.observers:
            i.modelischanged()
    def removeobserver(self, inobserver):
        '''Удаляемся из наблюдателей '''
        self.observers.remove(inobserver)

class Model(Observer):#паттерн наблюдатель
    '''Шаблон модели справочника. Расширяем наблюдателя до поддержки ORM
    
    Свойства:        
    
        * session (:class:`dbunit.dbuinit.engine`) - объект работы с ORM
    '''
    session = None
    def __init__(self):
        Observer.__init__(self)
        engine = SinglEngine()
        self.session = engine.getsession()

#=========================================================================
class ModelHandbook(Model): 
    '''Шаблон модели справочника
    
    Свойства:        
    
        * data (list) - список сырых записей
        * dataview (list in list) - двумерная таблица строк для отображения в view 
 
    '''
    data = []       #внутренняя структура данных связанных с базой
    dataview = []   #данные для вида в виде таблицы
    
    def getforview(self, record):
        '''Абстрактный метод. Возвращаем преобразование записи в виде строк для view. 
        
        Пример::
           
              return [record.name]'''
        raise  Exception("Need initialization method getforview in model")
    def getquery(self):
        '''Абстрактный метод. Возвращаем подготовленную сессию запроса для получения 
        списка значений (ORM  sqlalchemy).
        
        Пример:: 
        
            return self.session.query(Marka).order_by(Marka.name)'''
        raise  Exception("Need initialization method getquery in model")
    def getclassdb(self):
        '''Абстрактный метод. Возвращаем объект для добавления записи.
        
        Пример::
        
            return Marka()'''
        raise Exception("Need initialization method getclassdb in model")
    def update(self, row, col, value):
        'обновляем значение после редактирования'
        raise  Exception("Need initialization method update in model")
    def gettypecolumns(self):
        'типы колонок для вида'
        raise  Exception("Need initialization method gettypecolumns in model")
    def getheader(self):
        'имена заголовков'
        raise  Exception("Need initialization method getheader in model")
    
    def addrecord(self):
        'Добавить запись'
        self.currec = self.getclassdb()
        self.session.add(self.currec)
        self.session.commit()
        self.data += [self.currec]
        tmp = self.getforview(self.currec)
        self.dataview += [tmp]
        self.notifyobserver()
 
    def readrecords(self):
        'Читаем записи'
        self.data = []
        self.dataview = []
        query = self.getquery()
        for row in query:
            self.data += [row]
            tmp = self.getforview(row)
            self.dataview += [tmp]
        self.notifyobserver()    

    def rmrecord(self, row):
        'Удалить запись'
        item = self.data[row]
        self.session.delete(item)
        self.session.commit()
        del self.data[row]
        del self.dataview[row]
        self.notifyobserver()
    
    def rejectadd(self):#отмена удаления - сбрасываем последний элемент
        'Сбросить добавление'
        self.data = self.data[:-1]
        self.dataview = self.dataview[:-1]
        self.session.delete(self.currec)
        self.session.commit()
        self.notifyobserver()
    
#===============================================================================
# Отчеты
#===============================================================================
class ModelReport(Model):
    '''Шаблон модели отчета '''
        

#===============================================================================
# Журналы
#===============================================================================
class ModelJournal(Model):
    '''Шаблон модели журнала '''
    data = []
    dataview = []
    
    def getlistclass(self):
        raise  Exception("Need initialization method getheader in model")
    
    def header(self):#имена заголовков
        return u'Дата', u'Документ', u'Номер'
    
    def readrecords(self):
        self.data = []
        self.dataview = []
        
        for cl in self.getlistclass():
            query = self.session.query(cl).order_by(cl.docdate).all()
            for row in query:
                self.data += [[row.docdate, row]]
        self.data.sort()
        for datarow in self.data:
            dt = datarow[0].strftime("%d.%m.%Y %H:%M")
            self.dataview += [[dt, datarow[1].docname, datarow[1].docnumber]]
        self.notifyobserver()
        
    