#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore, QtSvg
from datetime import datetime, date, time
from bs4 import BeautifulSoup 
from pyPdf import PdfFileWriter, PdfFileReader
import cairosvg

class PrintQt(QtGui.QWidget):
    '''Печать данных'''
    pagesize = QtGui.QPrinter.A3
    orientation = QtGui.QPrinter.Portrait
    filetemplate = '' 
    data = {}
    
    def signals(self):
        self.connect(self.table,QtCore.SIGNAL("cellClicked(int ,int )"),self.paint1)
        self.connect(self.table,QtCore.SIGNAL("printing()"),self.paint2)
    
    def printpreview(self):
        'Старт печати с предпросмотром'
        self.printer = QtGui.QPrinter()
        self.printer.setOrientation(self.orientation)
        self.printer.setFullPage(True)
        self.printer.setPageSize(self.pagesize)
        self.printer.setOrientation(self.orientation)
        pp = QtGui.QPrintPreviewDialog(self.printer)
        pp.connect(pp, QtCore.SIGNAL('paintRequested(QPrinter*)'), self.previewprinting)        
        pp.exec_()
    
    def setsettings(self, pagesize = 4, portrait = True):
        'Установка настроек'
        if pagesize == 4:
            self.pagesize = QtGui.QPrinter.A4
        elif pagesize == 3:
            self.pagesize = QtGui.QPrinter.A3
        if portrait:
            self.orientation = QtGui.QPrinter.Portrait
        else:
            self.orientation = QtGui.QPrinter.Landscape
    
    def getsvgrender(self):
        'Рендер шаблона на основае переданных ключей в data и имени файла filetemplate'
        f = open(self.filetemplate, "r")
        svg = f.read()
        soup = BeautifulSoup(svg)
        tags = soup.findAll("text")
        for tag in tags:
            if tag["id"] in self.data.keys():
                span = tag.tspan
                spr = self.data[tag["id"]]
                #for i in self.data[tag["id"]]:
                #    spr = "%s %s"%(spr,i)
                span.string =spr
        f.close()
        return str(soup)
        
    
    def printing(self):
        '''Вывод на печать подменяя данные в шаблоне
            
            * filetemplate - имя файла шаблона
            * data  - список пар ключ и его значение в шаблоне'''

        self.printer = QtGui.QPrinter()
        self.printer.setOrientation(self.orientation)
        self.printer.setFullPage(True)
        self.printer.setPageSize(self.pagesize)
        self.printer.setOrientation(self.orientation)
        
        painter = QtGui.QPainter()
        painter.begin(self.printer)
        bar = self.getsvgrender()
        render = QtSvg.QSvgRenderer(QtCore.QByteArray(bar))
        render.render(painter)
        painter.end()
    
    def previewprinting(self, qprinter):
        '''Печать с предпросмотром '''
        painter = QtGui.QPainter()
        bar = self.getsvgrender()
        render = QtSvg.QSvgRenderer(QtCore.QByteArray(bar))
        
        painter.begin(qprinter)
        render.render(painter)
        painter.end()
        
#    def pppreviewneeds(self, qprinter):
#        print "pppreviewneeds"
#        t = time.time()
#        s2 = self.readsvg.readsvg('img/hp2.svg')
#        render1 = QtSvg.QSvgRenderer(QtCore.QByteArray(s2))
#        paint = QtGui.QPainter()
#        paint.begin(qprinter)
#        for i in range(self.spn1.value()):
#            if i>0: qprinter.newPage()
#            data = {}
#            data["number"] = i + self.spn2.value()
#            s1 = self.readsvg.readsvg('img/hp1.svg', data)
#            render = QtSvg.QSvgRenderer(QtCore.QByteArray(s1))
#            render.render(paint)
#            qprinter.newPage()
#            render1.render(paint)
#            print "page %d"%i, time.time()-t
#        paint.end()
#        print time.time()-t
#    
#    def print_(self): 
#        printer = QtGui.QPrinter(QtGui.QPrinter.HighResolution)
#        printer.setFullPage(True)
#        printer.setPageSize(QtGui.QPrinter.A3)
#        printer.setOrientation(QtGui.QPrinter.Portrait)
#    
#        workaround = 1
#        mnoj = 3
#        mnoj2 = 2
#    
#        painter = QtGui.QPainter()
#        painter.begin(printer)
#        #painter.translate(printer.paperRect().x() + printer.pageRect().width()/2,
#        #                    printer.paperRect().y() + printer.pageRect().height()/2)
#                
#        painter.setWindow(0,0,210 * mnoj,297*mnoj)
#        painter.setViewport(0, 0, printer.width() * workaround, printer.height() * workaround)
#        #n1 = int(n.hour/10)
#        #n2 = n.hour - int(n.hour/10)*10
#        #n3 = int(n.minute/10)
#        #n4 = n.minute - int(n.minute/10)*10
#        painter.setFont(QtGui.QFont("Arial",7,QtGui.QFont.Normal));
#        a = "%02d%02d%02d%02d"%(dt.day,dt.month,dt.hour,dt.minute)
#        s = "%s %s . %s %s   %s %s : %s %s"%(a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7])
#        painter.drawText(x*mnoj2*2,y*mnoj2,s)
#        print u'Печатаем'
#        painter.end()
    
