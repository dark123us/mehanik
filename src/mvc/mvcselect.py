#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from control import Control
from datetime import datetime, date
from dbunit.dbunit import SinglEngine
from model import Model
from view import View

class ModelSelect(Model):
    '''Шаблон модели выбора - подбора. Для подбора создаем список данных, которые выбираем
    
    Свойства::
        * data = [] - список записей ORM таблицы документа
        * dataview = [] - двумерная таблица со значениями для таблицы документа для view
        
    '''
    data = []
    dataview = []
    
    def getquery(self):
        '''.. warning::
        
            Абстрактный метод, возвращем запрос сессии для получения данных. 
        
        Вызывается из :func:`readdata`
        
        Пример::
        
            return self.session.query(DocumentServiceTo1To2)
                .filter(DocumentServiceTo1To2.id == docid).first()'''
        raise  Exception("Need initialization method getquery in document model")
    
    def datatodataview(self, record):
        '''.. warning::
        
            Абстрактный метод, возвращаем преобразование записи в виде строки для вида.
        
        Вызывается из :func:`readdata`
        
        Пример::
        
            dt = record.dt.strftime("%d.%m.%Y")
            return [record.bus.name, dt, record.typeservice.name]'''
        raise  Exception("Need initialization method datatodataview in model")

    def header(self):
        '''.. warning::
        
            Абстрактный метод возвращаем список имен заголовков таблицы
        
        Пример::
        
            return (u'Гаражный', u'Тип обслуживания', u'Дата проведения')''' 
        raise  Exception("Need initialization method header in model")
    
    def getselectdata(self, selectrows):
        'Возвращаем данные по выбранным строкам'
        data = []
        for d in self.data:
            data.append(d)
        return data 

    def readdata(self):
        'Читаем данные из ORM. Для работы необходимо определить :func:`getquery` и :func:`datatodataview`'
        rows = self.getquery()
        self.data = []
        self.dataview = []
        for rec in rows:
            self.data.append(rec)
            self.dataview.append(self.datatodataview(rec))
        self.notifyobserver()

class ViewSelect(View):
    '''Абстрактный класс вида документа (шаблон)
    
    Свойства::
        * windowtitle - заголовок окна '''
    windowtitle = u'Документ'
    
    def initwidget(self):
        'Используемые по умолчанию  виджеты'
        self.table = QtGui.QTableWidget()
        self.table.setEditTriggers(QtGui.QAbstractItemView.EditTriggers(0))
        self.dbutton = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok |QtGui.QDialogButtonBox.Cancel)
    
    def initlayout(self):
        'Расположение виджетов по умолчанию'
        grl = QtGui.QGridLayout()
        grl.addWidget(self.table)
        grl.addWidget(self.dbutton)
        self.setLayout(grl)
    
    def redraw(self, data):
        'Перерисовываем табличные данные'
        self.currowcol = [self.table.currentRow(), self.table.currentColumn()] 
        self.table.setRowCount(0)
        self.table.setRowCount(len(data))
        for row, rowdata in enumerate(data):
            for col, item in enumerate(rowdata):
                if type(item) == int or type(item) == long:
                    s = "%d"%item
                elif type(item) == str or type(item) == unicode:
                    s = item
                else:
                    s = ''
                self.itemtable(row,col,s)
        self.table.resizeRowsToContents()
        self.table.resizeColumnsToContents()
        self.table.setCurrentCell(self.currowcol[0], self.currowcol[1])
    
    def itemtable(self,row,col,s):
        'Определяем как отрисовывается ячейка в таблице'
        item = QtGui.QTableWidgetItem("%s"%s)
        if col == 0:
            item.setCheckState(0)
        self.table.setItem(row, col, item)
    
    def setheader(self, header):#установка имен заголовков
        'Выводим заголовки таблицы'
        #header = ["#"] + header 
        self.table.setColumnCount(len(header))
        self.table.setHorizontalHeaderLabels(header)
        self.table.resizeColumnsToContents()
    
    def getcheckedrows(self):
        checked = []
        rows = self.table.rowCount()
        for row in range(rows):
            if self.table.item(row,0).checkState() == 2:
                checked.append(row)
        return checked
        
class ControlSelect(Control):
    'Контрол для подборов'
    def __init__(self, model, view, parent= None):
        Control.__init__(self, model, view, parent)
        view.setheader(self.model.header())
        model.readdata()
        #self.view.connect(self.view.table, QtCore.SIGNAL("cellChanged ( int, int )"), self.cellchanged)
        #self.view.connect(self.view.table, QtCore.SIGNAL("cellDoubleClicked ( int, int )"), self.dubleclicked)
        #self.view.connect(self.view.a_add, QtCore.SIGNAL('triggered()'), self.addrecord)
        #self.view.connect(self.view.a_rm, QtCore.SIGNAL('triggered()'), self.rmrecord)        
    ''
    def showdialog(self):
        'Установка подбора '
        self.view.dialog = QtGui.QDialog()
        self.view.dialog.setMinimumWidth(600)
        gr = QtGui.QGridLayout()
        gr.addWidget(self.view)
        self.view.dialog.setLayout(gr)
        self.view.dialog.setWindowTitle(self.view.windowtitle)
        #self.view.dialog.setMinimumSize(500,500)
        self.view.dialog.connect(self.view.dbutton, QtCore.SIGNAL("rejected()"), self.view.dialog.reject) 
        self.view.dialog.connect(self.view.dbutton, QtCore.SIGNAL("accepted()"), self.view.dialog.accept)
        self.view.dialog.connect(self.view.dialog, QtCore.SIGNAL("rejected()"), self.rejectdialog)
        self.view.dialog.connect(self.view.dialog, QtCore.SIGNAL("accepted()"), self.acceptdialog)
        self.view.dialog.setModal(True)
        self.view.dialog.show()
    
    def setaccept(self, proc):
        'Устанавливаем сигнал, для вызывающего подбор по нажатию кнопки Ok'
        self.view.dialog.connect(self.view.dialog, QtCore.SIGNAL("accepted()"), proc)
    
    def modelischanged(self):
        'Обработка сообщения от модели об ее изменении (паттерн наблюдатель)'
        self.view.table.blockSignals(True)
        self.view.redraw(self.model.dataview)
        self.view.table.blockSignals(False)
    
    def rejectdialog(self):
        'Обработка отмены побдобра'
    
    def acceptdialog(self):
        'Обработка подбора, пользователь нажал Ok'
    
    def getdataselect(self):
        checkedrows = self.view.getcheckedrows()
        data = self.model.getselectdata(checkedrows)
        return data 
        
    