#!/usr/bin/python
# -*- coding: UTF-8  -*-
from dbunit.dbunit import ServiceTo1To2, Marka 
from mvc.model import ModelHandbook  
from mvc.view import ViewHandbook
from  mvc.control import ControlHandbook
from marka import ModelMarka, ViewMarka, ControlMarka

class ModelServiceTo1To2(ModelHandbook):
    'Справочник ТО-1 ТО-2'
    name = u'ТО-1, ТО-2'
    
    def header(self):
        return [u'Марка автобуса', u'ТО-1 (км)', u'ТО-2 (км)']
    
    def update(self, row, column, value):
        if column == 0:
            self.data[row].markaid = value
            self.session.commit()
            name = self.data[row].marka.name
            self.dataview[row][column] = name
            self.notifyobserver()
        elif column == 1:
            try:
                self.data[row].to1km = int(value)
                self.dataview[row][column] = str(value)
                self.session.commit()
            except:
                print "ERROR in update",row, column, value
        elif column == 2:
            self.data[row].to2km = value
            self.dataview[row][column] = str(value)
            self.session.commit()
    
    def getforview(self, value):
        if value.markaid == 0:
            return ['', value.to1km, value.to2km]
        else:
            return [value.marka.name, value.to1km, value.to2km]
    
    def getquery(self):
        return self.session.query(ServiceTo1To2).join(Marka).order_by(Marka.name)
    
    def gettypecolumns(self):
        return (ModelMarka, int, int)
    
    def getclassdb(self):
        return ServiceTo1To2()

class ViewServiceTo1To2(ViewHandbook):
    'Визуальная часть справочника ТО-1 ТО-2'
    windowtitle = u'Справочник ТО-1 ТО-2'


class ControlServiceTo1TO2(ControlHandbook):
    'Реализация ТО-1 ТО-2'
    dialogname = u'ТО-1 ТО-2'
    def __init__(self, parent=None):
        view = ViewServiceTo1To2()
        model = ModelServiceTo1To2()
        ControlHandbook.__init__(self, model, view, parent=parent)
    
    def getvaluedict(self, value):
        if value.toPyObject() == ModelMarka:#в данном случае HMMarka является обязательной, следовательно, 
            #если будет режект или не выбрано ничего отменить добавление
            self.selecthandbook(ModelMarka, ViewMarka, ControlMarka)
    

    
