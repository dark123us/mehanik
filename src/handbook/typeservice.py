#!/usr/bin/python
# -*- coding: UTF-8  -*-
from mvc.model import ModelHandbook
from mvc.view import ViewHandbook
from mvc.control import ControlHandbook
from dbunit.dbunit import TypeService


class ModelTypeService(ModelHandbook):
    'Справочник типов сервиса'
    name = u'тип сервиса'
    def getclassdb(self):
        return TypeService()
    
    def gettypecolumns(self):
        return (str, str)
    
    def header(self):
        return [u'Название', u'Полное название']
        
    def getforview(self, record):
        return [record.name, record.fullname]
    
    def getquery(self):
        return self.session.query(TypeService).order_by(TypeService.name)
    
    def update(self, row, col, value):
        if col == 0:
            self.data[row].name = value
            self.dataview[row][col] = value
        elif col == 1:
            self.data[row].fullname = value
            self.dataview[row][col] = value
        self.session.commit()
        self.notifyobserver()

class ViewTypeService(ViewHandbook):
    'Визуальная часть справочника автобусов'
    windowtitle = u'тип обслуживания'

class ControlTypeService(ControlHandbook):
    'Контрол типа обслуживания'

    
    def __init__(self, model = None, view = None, parent= None):
        if not model:
            model = ModelTypeService ()
        if not view:
            view = ViewTypeService()
        ControlHandbook.__init__(self, model, view, parent=parent)
    
    
    
