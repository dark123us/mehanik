#!/usr/bin/python
# -*- coding: UTF-8  -*-
from mvc.model import ModelHandbook
from mvc.view import ViewHandbook
from mvc.control import ControlHandbook
from dbunit.dbunit import Marka

class ModelMarka(ModelHandbook):
    'Справочник марок'
    name = u'марки транспорта'
    def getclassdb(self):
        return Marka()
    
    def gettypecolumns(self):
        return (str,)
    
    def header(self):
        return [u'Марка автобуса']
        
    def getforview(self, value):
        return [value.name]
    
    def getquery(self):
        return self.session.query(Marka).order_by(Marka.name)
    
    def update(self, row, col, value):
        #print row, col, value
        if col == 0:
            self.data[row].name = value
            self.dataview[row][0] = value
        self.session.commit()

class ViewMarka(ViewHandbook):
    'Визуальная часть справочника марки автобусов'

class ControlMarka(ControlHandbook):
    'Марка автобуса'
    dialogname = u'Марка автобуса'
