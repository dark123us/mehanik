#!/usr/bin/python
# -*- coding: UTF-8  -*-
from mvc.model import ModelHandbook
from mvc.view import ViewHandbook
from mvc.control import ControlHandbook
from dbunit.dbunit import Bus, Marka
from marka import ModelMarka, ViewMarka, ControlMarka

class ModelBus(ModelHandbook):
    'Справочник автобусов'
    name = u'автобусы'
    def getclassdb(self):
        return Bus()
    
    def gettypecolumns(self):
        return (int, str, Marka)
    
    def header(self):
        return [u'Гаражный номер', u'Гос.номер', u'Марка автобуса']
        
    def getforview(self, record):
        if record.id == 0:
            return ["0", "", ""]
        else:
            if record.markaid>0:
                marka = record.marka.name
            else:
                marka = ''
            return [str(record.garnm), record.gosnm, marka]
    
    def getquery(self):
        return self.session.query(Bus).order_by(Bus.garnm)
    
    def update(self, row, col, value):
        if col == 0:
            try:
                self.data[row].garnm = int(value)
                self.dataview[row][col] = value
                self.session.commit()
            except:
                print "ERROR in update",row, col, value
        elif col == 1:
            self.data[row].gosnm = value
            self.dataview[row][col] = value
        elif col == 2:
            self.data[row].markaid = value
            self.session.commit()
            name = self.data[row].marka.name
            self.dataview[row][col] = name
            self.notifyobserver()
        self.session.commit()

class ViewBus(ViewHandbook):
    'Визуальная часть справочника автобусов'
    windowtitle = u'Справочник автобусов'

class ControlBus(ControlHandbook):
    'Контроль справочника автобусов'
    dialogname = u'Марка автобуса'
    
    def __init__(self, model = None, view = None, parent= None):
        if not model:
            model = ModelBus()
        if not view:
            view = ViewBus()
        ControlHandbook.__init__(self, model, view, parent=parent)
    
    def getvaluedict(self, value):
        if value.toPyObject() == Marka:#в данном случае HMMarka является обязательной, следовательно, 
            #если будет режект или не выбрано ничего отменить добавление
            self.selecthandbook(ModelMarka, ViewMarka, ControlMarka)
    
