#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from dbunit.dbunit import SinglEngine
import sys
from handbook.serviceto1to2 import ControlServiceTo1TO2 as HandbookServiceTo1TO2 
from handbook.bus import ControlBus as HandbookBus
from document.serviceto1to2 import ControlServiceTo1To2 as DocServiceTo1To2
from document.readyserviceto1to2 import ControlReadyServiceTo1To2 as DocReadyServiceTo1To2
from report.mehanik import ControlMehanik 
from journal.all import ControlAllJournal

class MainMDiWindow(QtGui.QMainWindow):
    def __init__(self, *args, **kwargs):
        QtGui.QMainWindow.__init__(self, *args, **kwargs)
        self.initvalue()
        self.initwidget()
        self.createactions()
        self.createmenus()
        self.createtoolbars()
        self.createstatusbar()
        self.updatemenus()
        self.readsettings()
        self.setcentral()
        self.setWindowTitle(u'Механик')
    
    def initvalue(self):
        self.centralarea = QtGui.QMdiArea()
        self.windowmapper = QtCore.QSignalMapper()
    
    def initwidget(self):
        #self.centralarea.addSubWindow(self.control.getwidget())
        self.setCentralWidget(self.centralarea)
        self.centralarea.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.centralarea.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        
    def initmenu(self):
        self.setMenuWidget()
    
    def initaction(self):
        self.connect(self.centralarea, QtCore.SIGNAL("subWindowActivated(QMdiSubWindow*)"), self.updatemenus)
        self.connect(self.windowmapper, QtCore.SIGNAL("mapped(QWidget*)"), self.setactivesubwindow)
        #self.connect(self.windowmapper, QtCore.SIGNAL("mapped(QObject *)"), self.setactivesubwindow)
        #self.connect(self.windowmapper, QtCore.SIGNAL("mapped(int)"), self.setactivesubwindow)
    
    def setcentral(self):
        screen = QtGui.QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((screen.width()-size.width())/2,
            (screen.height()-size.height())/2)
    
    def updatewindowmenu(self):
        self.windowmenu.clear()
        self.windowmenu.addAction(self.closeAct);
        self.windowmenu.addAction(self.closeAllAct)
        self.windowmenu.addSeparator();
        self.windowmenu.addAction(self.tileAct);
        self.windowmenu.addAction(self.cascadeAct)
        self.windowmenu.addSeparator()
        self.windowmenu.addAction(self.nextAct)
        self.windowmenu.addAction(self.previousAct)
        self.windowmenu.addAction(self.separatorAct)
        windows = self.centralarea.subWindowList()
        self.separatorAct.setVisible(not windows)
        for i in range(len(windows)):
            child = windows[i].widget()
            if i < 9:
                text = u"&{0} {1}".format((i + 1), child.windowtitle)
            else:
                text = u"{0} {1}".format((i + 1), child.windowtitle)
            action  = self.windowmenu.addAction(text)
            action.setCheckable(True)
            action.setChecked(child == self.activemdichild())
            self.connect(action, QtCore.SIGNAL("triggered()"), self.windowmapper.map)
            self.windowmapper.setMapping(action, windows[i])
            #FIXME нужно разобраться с не работающим слотом map
    
    
    def updatemenus(self):
        hasmdichild = (self.activemdichild() != 0)
        self.separatorAct.setVisible(hasmdichild)
        if self.activemdichild():
            hasselection = self.activemdichild().textCursor().hasSelection()

    def createactions(self):
        self.hbusAct = QtGui.QAction(u"Автобусы", self)
        self.hbusAct.setStatusTip(u"Справочник Автобусов")
        self.connect(self.hbusAct, QtCore.SIGNAL("triggered()"), self.openhandbookbus)
        
        self.hserviceto1to2Act = QtGui.QAction(u"ТО-1 ТО-2", self)
        self.hserviceto1to2Act.setStatusTip(u"Справочник ТО-1 ТО-2")
        self.connect(self.hserviceto1to2Act, QtCore.SIGNAL("triggered()"), self.openhandbook)
        
        self.dserviceto1to2Act = QtGui.QAction(u"Планирование ТО-1 ТО-2", self)
        self.dserviceto1to2Act.setStatusTip(u"Планирование ТО-1 ТО-2")
        self.connect(self.dserviceto1to2Act, QtCore.SIGNAL("triggered()"), self.opendocument)
        
        self.dreadyserviceto1to2Act = QtGui.QAction(u"Прохождение ТО-1 ТО-2", self)
        self.dreadyserviceto1to2Act.setStatusTip(u"Прохождение ТО-1 ТО-2")
        self.connect(self.dreadyserviceto1to2Act, QtCore.SIGNAL("triggered()"), self.opendocument2)
        
        self.rmehanikAct = QtGui.QAction(u"Механик", self)
        self.rmehanikAct.setStatusTip(u"Механик")
        self.connect(self.rmehanikAct, QtCore.SIGNAL("triggered()"), self.openmehanik)
        
        self.alljournalAct = QtGui.QAction(u"Все документы", self)
        self.alljournalAct.setStatusTip(u"Все документы")
        self.connect(self.alljournalAct, QtCore.SIGNAL("triggered()"), self.openalljournal)
        
        self.exitAct = QtGui.QAction(u"E&xit", self)
        self.exitAct.setShortcuts(QtGui.QKeySequence.Quit)
        self.exitAct.setStatusTip(u"Exit the application")
        self.connect(self.exitAct, QtCore.SIGNAL("triggered()"), self.closeallwindows)
        
        self.closeAct = QtGui.QAction(u"Cl&ose", self)
        self.closeAct.setStatusTip(u"Close the active window")
        self.connect(self.closeAct, QtCore.SIGNAL("triggered()"), self.centralarea.closeActiveSubWindow)
        
        self.closeAllAct = QtGui.QAction(u"Close &All", self)
        self.closeAllAct.setStatusTip(u"Close all the windows")
        self.connect(self.closeAllAct, QtCore.SIGNAL("triggered()"), self.centralarea.closeAllSubWindows);
             
        self.tileAct = QtGui.QAction(u"&Tile", self)
        self.tileAct.setStatusTip(u"Tile the windows")
        self.connect(self.tileAct, QtCore.SIGNAL("triggered()"), self.centralarea.tileSubWindows)

        self.cascadeAct = QtGui.QAction(u"&Cascade", self)
        self.cascadeAct.setStatusTip(u"Cascade the windows")
        self.connect(self.cascadeAct, QtCore.SIGNAL("triggered()"), self.centralarea.cascadeSubWindows)

        self.nextAct = QtGui.QAction(u"Ne&xt", self)
        self.nextAct.setShortcuts(QtGui.QKeySequence.NextChild);
        self.nextAct.setStatusTip(u"Move the focus to the next window")
        self.connect(self.nextAct, QtCore.SIGNAL("triggered()"), self.centralarea.activateNextSubWindow)

        self.previousAct = QtGui.QAction(u"Pre&vious", self)
        self.previousAct.setShortcuts(QtGui.QKeySequence.PreviousChild)
        self.previousAct.setStatusTip(u"Move the focus to the previous window")
        self.connect(self.previousAct, QtCore.SIGNAL("triggered()"), self.centralarea.activatePreviousSubWindow)

        self.separatorAct = QtGui.QAction(self)
        self.separatorAct.setSeparator(True)

    def openhandbook(self):
        handbook = HandbookServiceTo1TO2()
        #control.getwidget().setWindowIconText(u'Справочник')
        self.centralarea.addSubWindow(handbook.widget());
        handbook.show()
        #self.updatewindowmenu()
    
    def openhandbookbus(self):
        handbook = HandbookBus()
        self.centralarea.addSubWindow(handbook.widget());
        handbook.show()
        
    def opendocument(self):
        document = DocServiceTo1To2()
        self.centralarea.addSubWindow(document.widget());
        document.show()

    def opendocument2(self):
        document = DocReadyServiceTo1To2()
        self.centralarea.addSubWindow(document.widget());
        document.show()
    
    def openmehanik(self):
        mehanik = ControlMehanik()
        self.centralarea.addSubWindow(mehanik.widget());
        mehanik.show()
    
    def openalljournal(self):
        journal = ControlAllJournal(self)
        self.centralarea.addSubWindow(journal.widget());
        journal.show() 
    
    def createmenus(self):
        filemenu = QtGui.QMenu()
        filemenu = self.menuBar().addMenu(u'File')
        filemenu.addAction(self.exitAct)
        
        handbookmenu = QtGui.QMenu()
        handbookmenu = self.menuBar().addMenu(u'Справочники')
        handbookmenu.addAction(self.hbusAct)
        handbookmenu.addAction(self.hserviceto1to2Act)
        
        
        documentmenu = QtGui.QMenu()
        documentmenu = self.menuBar().addMenu(u'Документы')
        documentmenu.addAction(self.dserviceto1to2Act)
        documentmenu.addAction(self.dreadyserviceto1to2Act)
        
        journalmenu = QtGui.QMenu()
        journalmenu = self.menuBar().addMenu(u'Журналы')
        journalmenu.addAction(self.alljournalAct)
        
        reportmenu = QtGui.QMenu()
        reportmenu = self.menuBar().addMenu(u'Отчеты')
        reportmenu.addAction(self.rmehanikAct)
        
        self.windowmenu = self.menuBar().addMenu(u'Window')
        self.updatewindowmenu()
        self.connect(self.windowmenu, QtCore.SIGNAL("aboutToShow()"), self.updatewindowmenu)
    
    def createtoolbars(self):
        pass
#         fileToolBar = addToolBar(tr("File"));
#     fileToolBar->addAction(newAct);
#     fileToolBar->addAction(openAct);
#     fileToolBar->addAction(saveAct);
#
#     editToolBar = addToolBar(tr("Edit"));
#     editToolBar->addAction(cutAct);
#     editToolBar->addAction(copyAct);
#     editToolBar->addAction(pasteAct);
    
    def createstatusbar(self):
        self.statusBar().showMessage(u"Ready")
        
    def readsettings(self):
        pass
#         QSettings settings("Trolltech", "MDI Example");
#     QPoint pos = settings.value("pos", QPoint(200, 200)).toPoint();
#     QSize size = settings.value("size", QSize(400, 400)).toSize();
#     move(pos);
#     resize(size);
    
    
    def writesettings(self):
        pass
#         QSettings settings("Trolltech", "MDI Example");
#     settings.setValue("pos", pos());
#     settings.setValue("size", size());
    
    def closeEvent(self, event):
        self.centralarea.closeAllSubWindows()
        if (self.centralarea.currentSubWindow()): 
            event.ignore()
        else:
            self.writesettings()
            event.accept()
    
    def closeallwindows(self):
        self.close()
    
    def activemdichild(self):
        activesubwindow = self.centralarea.activeSubWindow()
        if activesubwindow:
            return activesubwindow.widget()
        return 0

#
# QMdiSubWindow *MainWindow::findMdiChild(const QString &fileName)
# {
#     QString canonicalFilePath = QFileInfo(fileName).canonicalFilePath();
#
#     foreach (QMdiSubWindow *window, mdiArea->subWindowList()) {
#         MdiChild *mdiChild = qobject_cast<MdiChild *>(window->widget());
#         if (mdiChild->currentFile() == canonicalFilePath)
#             return window;
#     }
#     return 0;
# }
#
# void MainWindow::switchLayoutDirection()
# {
#     if (layoutDirection() == Qt::LeftToRight)
#         qApp->setLayoutDirection(Qt::RightToLeft);
#     else
#         qApp->setLayoutDirection(Qt::LeftToRight);
# }
#
    def setactivesubwindow(self, window = None):
        print "setactivesubwindow"
        if window:
            self.centralarea.setActiveSubWindow(window)


def MainQt():
    engine = SinglEngine()
    engine.setconnect(typedbasename = 'mysql+mysqldb',
                              host = '10.1.12.52', 
                              dbname = 'mehaniktest', 
                              user = 'mehaniktest', 
                              password = '12345',
                              echo = False) 
                              #echo = True)
    #engine.createdatabase()#пересоздаем таблицы и тестовые данные
    
    
    app = QtGui.QApplication(sys.argv)
    mw = MainMDiWindow()#MainWindow()
    mw.show()
    sys.exit(app.exec_())