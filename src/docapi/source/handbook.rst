***********
Справочники
***********

Марка автобуса
==============

.. automodule:: handbook.marka
    :members:

Автобусы
=================

.. automodule:: handbook.bus
    :members:

Обслуживание ТО-1 ТО-2
=======================

.. automodule:: handbook.serviceto1to2
    :members:

Типы обслуживания
=================

.. automodule:: handbook.typeservice
    :members:

