************************************************************
MVC шаблоны для справочников, документов, журналов и отчетов 
************************************************************

Модель
======

.. automodule:: mvc.model
    :members:

Вид
===

.. automodule:: mvc.view
    :members:   

Контроллер
==========
.. automodule:: mvc.control
    :members:


MVC Документа
================

.. automodule:: mvc.mvcdocument
   :members:
   
MVC Подбора
===========

.. automodule:: mvc.mvcselect
   :members: