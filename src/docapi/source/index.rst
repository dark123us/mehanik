.. mehanik documentation master file, created by
   sphinx-quickstart on Sat Mar  9 09:53:12 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Документация по API АСУП АП-1
===================================

Содержание:

.. toctree::
   	:maxdepth: 2

	dbunit.rst
	
   	mvc.rst
   
   	handbook.rst

	document.rst
	
	journal.rst
	
	report.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

