#!/usr/bin/python
# -*- coding: UTF-8  -*-

from mvc.mvcdocument import ModelDocument, ViewDocument, ControlDocument

from handbook.bus import ModelBus, ViewBus, ControlBus
from handbook.typeservice import ModelTypeService, ViewTypeService, ControlTypeService

from dbunit.dbunit import DocumentReadyServiceTo1To2, Bus, DocTableReadyServiceTo1To2, TypeService
from datetime import datetime, date
from PyQt4 import QtGui, QtCore

class ModelReadyServiceTo1To2(ModelDocument):
    'Документ отметки прохождения ТО-1 или ТО-2'
    def getclassdoc(self): #класс отвечающий за основные поля документа
        return DocumentReadyServiceTo1To2()
    
    def getclasstable(self, docid):
        'Определяем родительский метод - возвращаем ORM класс таблицы документа'
        rec = DocTableReadyServiceTo1To2(docid)
        rec.busid = 1
        rec.typeserviceid = 1
        rec.dt = date.today()
        rec.km = 0
        return rec
    
    def getquerydoc(self, docid):
        'Определяем родительский метод - возвращаем ORM запрос данных по документу'
        return self.session.query(DocumentReadyServiceTo1To2)\
            .filter(DocumentReadyServiceTo1To2.id == docid).first()
    
    def getquerytable(self, docid):
        'Определяем родительский метод - возвращаем ORM запрос данных по таблице документа'
        return self.session.query(DocTableReadyServiceTo1To2)\
                    .join(Bus).join(TypeService)\
                    .filter(DocTableReadyServiceTo1To2.docid == docid).all()       
     
    def datatoview(self, record):
        'Определяем как представить запись таблицы для вида'
        try:
            busname = record.bus.garnm
        except:
            busname = ''
        try:
            dt = self.datetostr(record.dt)
        except:
            dt = ''
        try:
            typeservice = record.typeservice.name
        except:
            typeservice = ''
        km = record.km
        return [busname, typeservice, dt, km]
    
    
    def header(self):
        'Переопределяем заголовок таблицы'
        return (u'Гаражный', u'Тип обслуживания', u'Дата проведения', u'Километраж')
    
    def gettypecolumns(self):
        'Переопределяем типы колонок таблицы'
        return (Bus, TypeService, date, int)
    
    def update(self, row, col, value):
        'Определяем действия на изменения данных в таблице'
        if col == 0:
            if value > 0:
                self.data[row].busid = value
                self.session.commit()
                name = self.data[row].bus.garnm
                self.dataview[row][col] = name
        elif col == 1:
            if value > 0:
                self.data[row].typeserviceid = value
                self.session.commit()
                name = self.data[row].typeservice.name
                self.dataview[row][col] = name
        elif col == 2:
            self.data[row].dt = value
            self.session.commit()
            self.dataview[row][col] = self.datetostr(self.data[row].dt)
        elif col == 3:
            self.data[row].km = value
            self.session.commit()
            self.dataview[row][col] = str(self.data[row].km)
        self.notifyobserver()
#===============================================================================
# 
#===============================================================================
class ViewReadyServiceTo1To2(ViewDocument):
    'Визуальная часть документа ТО-1 ТО-2'
    windowtitle = u'Прохождение ТО-1 ТО-2'

#===========================================================================
# 
#===========================================================================
class ControlReadyServiceTo1To2(ControlDocument):
    'Документ прохождения ТО-1 ТО-2'
    def __init__(self, opendocid = 0, parent=None):
        model = ModelReadyServiceTo1To2(opendocid)
        view = ViewReadyServiceTo1To2()
        ControlDocument.__init__(self, model, view, parent=parent)

    def modelischanged(self):
        'Реагирование на изменение модели'
        self.view.redraw(self.model.curdoc, self.model.dataview) 
    
    def getvaluedict(self, value):
        'Реагируем на начало редактирования'
        if value == Bus:
            self.selecthandbook(ModelBus, ViewBus, ControlBus)
        elif value == TypeService:
            self.selecthandbook(ModelTypeService, ViewTypeService, ControlTypeService)