#!/usr/bin/python
# -*- coding: UTF-8  -*-

from mvc.mvcdocument import ModelDocument, ViewDocument, ControlDocument
from mvc.mvcselect import ModelSelect, ViewSelect, ControlSelect

from handbook.bus import ModelBus, ViewBus, ControlBus
from handbook.typeservice import ModelTypeService, ViewTypeService, ControlTypeService
from datetime import datetime, date
from PyQt4 import QtGui, QtCore
from dbunit.dbunit import DocumentServiceTo1To2, DocTableServiceTo1To2, Bus, TypeService, Probeg, ServiceTo1To2
from sqlalchemy import func

from pprint import pprint

class ModelServiceTo1To2(ModelDocument):
    docname = u'Документ ТО-1, ТО-2'
    
    def getclassdoc(self): #класс отвечающий за основные поля документа
        'Определяем родительский метод - возвращаем ORM класс документа'
        return DocumentServiceTo1To2()
    
    def getclasstable(self, docid):
        'Определяем родительский метод - возвращаем ORM класс таблицы документа'
        rec = DocTableServiceTo1To2(docid)
        rec.busid = 1
        rec.typeserviceid = 1
        rec.dt = date.today()
        return rec

    def getquerydoc(self, docid):
        'Определяем родительский метод - возвращаем ORM запрос данных по документу'
        return self.session.query(DocumentServiceTo1To2)\
            .filter(DocumentServiceTo1To2.id == docid).first()
    
    def getquerytable(self, docid):
        'Определяем родительский метод - возвращаем ORM запрос данных по таблице документа'
        return self.session.query(DocTableServiceTo1To2)\
                    .join(Bus).join(TypeService)\
                    .filter(DocTableServiceTo1To2.docid == docid).all()
    
    def datatoview(self, record):
        'Определяем как представить запись таблицы для вида'
        try:
            busname = record.bus.garnm
        except:
            busname = ''
        try:
            dt = self.datetostr(record.dt)
        except:
            dt = ''
        try:
            typeservice = record.typeservice.name
        except:
            typeservice = ''
        return [busname, typeservice, dt]
    
    
    def header(self):
        return (u'Гаражный', u'Тип обслуживания', u'Дата проведения')
    
    def gettypecolumns(self):
        return (Bus, TypeService, date)
    
    def update(self, row, col, value):
        if col == 0:
            if value > 0:
                self.data[row].busid = value
                self.session.commit()
                name = self.data[row].bus.garnm
                self.dataview[row][col] = name
        elif col == 1:
            if value > 0:
                self.data[row].typeserviceid = value
                self.session.commit()
                name = self.data[row].typeservice.name
                self.dataview[row][col] = name
        elif col == 2:
            self.data[row].dt = value
            self.session.commit()
            self.dataview[row][col] = self.datetostr(self.data[row].dt)
        self.notifyobserver()
    
    def inserttable(self, listrec):
        self.cleartable()
        for rec in listrec:
            pass
#===============================================================================
# 
#===============================================================================
class ViewServiceTo1To2(ViewDocument):
    'Визуальная часть документа ТО-1 ТО-2'
    windowtitle = u'Документ ТО-1 ТО-2'
    
    def initwidget(self):
        'Добавляем определение кнопки подбора'
        ViewDocument.initwidget(self)
        self.selectbutton = QtGui.QPushButton(u'Подбор')
    
    def initlayout(self):
        'Переопределяем расположение элементов'
        grl = QtGui.QGridLayout()
        hl = QtGui.QHBoxLayout()
        hl.addWidget(QtGui.QLabel(u'Номер документа'))
        hl.addWidget(self.docnumber)
        hl.addWidget(QtGui.QLabel(u'от'))
        hl.addWidget(self.docdata)
        hl.addStretch(1)
        
        hl2 = QtGui.QHBoxLayout()
        hl2.addWidget(self.selectbutton)
        hl2.addStretch(1)
        
        grl.addLayout(hl, 0, 0)
        grl.addLayout(hl2, 1, 0)
        grl.addWidget(self.panel)
        grl.addWidget(self.table)
        grl.addWidget(self.dbutton)
        self.setLayout(grl)
#===========================================================================
# 
#===========================================================================
class ControlServiceTo1To2(ControlDocument):
    'Документ ТО-1 ТО-2'
    def __init__(self, opendocid = 0, parent=None):
        model = ModelServiceTo1To2(opendocid)
        view = ViewServiceTo1To2()
        ControlDocument.__init__(self, model, view, parent=parent)
        view.connect(view.selectbutton, QtCore.SIGNAL('clicked()'), self.podbor)
        #view.typecolumns =  model.gettypecolumns()
    
    def modelischanged(self):
        'Реагирование на изменение модели'
        self.view.redraw(self.model.curdoc, self.model.dataview) 
    
    def getvaluedict(self, value):
        'Реагируем на начало редактирования'
        if value == Bus:
            self.selecthandbook(ModelBus, ViewBus, ControlBus)
        elif value == TypeService:
            self.selecthandbook(ModelTypeService, ViewTypeService, ControlTypeService)
            
    def podbor(self):
        'Обработка подбора'
        self.podbordialog = ControlPodbor()
        self.podbordialog.showdialog()
        self.podbordialog.setaccept(self.acceptpodor)
    
    def acceptpodor(self):
        data = self.podbordialog.getdataselect()
        self.model.inserttable(data)
        
         
    
#===========================================================================
# 
#===========================================================================
class ModelPodbor(ModelSelect):
    'Модель подбора значений для документа при планировании'
#    def getquery(self):
#        return self.session.query(Bus).order_by(Bus.garnm).all()
#    def datatoview(self, record):
#        return [record.garnm]
    
    def header(self):
        return [u'Гаражный', u'Пробег км', u'Проведено ТО-1', u'Проведено ТО-2', u'ТО-1', u'ТО-2', ]
    
    def modelischanged(self):
        'Реагирование на изменение модели'
        self.view.redraw(self.model.dataview) 
    
    def readbus(self):
        'Возвращаем список автобусов в виде справочника - ключ id атобуса, значение - запись в ORM'
        datatmp = {}
        
        query = self.session.query(Bus).order_by(Bus.garnm).all()
        for rec in query:
            datatmp[rec.id] = {}
            datatmp[rec.id]["bus"] = rec
        return datatmp
    
    def readprobeg(self, datatmp):
        'Получаем пробег автобусов, дополняем значения автобусов'
        dtbegin = date(2012,12,31) #2012.12.31 в этот день заносим начальные пробеги за весь период эксплуатации
        dtend = date(2030,1,1)
        #TODO: получить километраж с начала эксплуатации
        query = self.session.query(Probeg.busid, func.sum(Probeg.km)).\
                    filter(Probeg.dt >= dtbegin,
                           Probeg.dt <= dtend).\
                    group_by(Probeg.busid).all()
        for rec in query:
            datatmp[rec.busid]["probeg"] = rec[1]
        return datatmp
    
    def readserviceto1to2(self):
        'К каждому автобусу по его марке привязываем сервисные пробеги'
        query = self.session.query(ServiceTo1To2).all()
        datasservice = {}
        for row in query:
            datasservice[row.markaid] = row
        return datasservice
    
    def readreadyserviceto1to2(self, datatmp):
        'Пробег при прохождении прошлого ТО-1 ТО-2'
        return datatmp
    
    def renderall(self, datatmp, dataservice):
        'Собираем таблицу для возможности сортировки'
        data = []
        for key in datatmp.keys():
            tmp = []
            markaid = 0
            marka = ''
            if datatmp[key]["bus"].marka:
                markaid = datatmp[key]["bus"].marka.id  
                marka = datatmp[key]["bus"].marka.name.strip() 
            garnm = datatmp[key]["bus"].garnm
            try:
                probeg = datatmp[key]["probeg"]         #2
            except KeyError:
                probeg = 0
            
            if markaid > 0:
                try:
                    km1 = dataservice[markaid].to1km 
                except KeyError:
                    km1 = 0
                try:
                    km2 = dataservice[markaid].to2km 
                except KeyError:
                    km2 = 0
            
            tmp.append(garnm) #0
            tmp.append(marka) #1
            tmp.append(probeg)#2
            tmp.append(0)     #3
            tmp.append(0)     #4
            tmp.append(km1)   #5
            tmp.append(km2)   #6
            data.append(tmp)
        return data
    
    def readdata(self):
        'Переопределили из-за сложного образования'
        self.data = []
        self.dataview = []
        datatmp = self.readbus() #получаем список автобусов
        datatmp = self.readprobeg(datatmp)#дополняем пробегом
        dataservice = self.readserviceto1to2()#получаем сервисные пробеги по маркам
        datatmp = self.readreadyserviceto1to2(datatmp)#Получаем пробеги с прошлого прохождения
        self.data = self.renderall(datatmp, dataservice)
        self.data = sorted(self.data ) 
        self.datatodataview()
        self.notifyobserver()
        
        
    def datatodataview(self):
        'Преобразоваываем данные в представление'
        self.dataview = []
        for row in self.data:
            tmp = []
            tmp.append(u"{0} ({1})".format(row[0], row[1]))#гаражный и марка
            tmp.append("%0.1f"%row[2]) #км
            tmp.append("%0.1f"%row[3]) #км ТО-1
            tmp.append("%0.1f"%row[4]) #км ТО-2
            tmp.append("%d"%row[5]) #км ТО-1
            tmp.append("%d"%row[6]) #км ТО-2
            self.dataview.append(tmp)
            #self.dataview.append([str(row[]), str(rec[1])])

class ViewPodbor(ViewSelect):
    'Вид подбора'
    windowtitle = u'Выбор автобусов на ТО'
    
    def itemtable(self,row,col,s):
        'Определяем как отрисовывается ячейка в таблице'
        item = QtGui.QTableWidgetItem("%s"%s)
        if col == 2 or col == 3:
            item.setCheckState(0)
        self.table.setItem(row, col, item)
    
    def getcheckedrows(self):
        checked = []
        rows = self.table.rowCount()
        for row in range(rows):
            if self.table.item(row,2).checkState() == 2:
                checked.append([row, 1])
            if self.table.item(row,3).checkState() == 2:
                checked.append([row, 2])
        return checked

class ControlPodbor(ControlSelect):
    'Контрол подбора'
    def __init__(self):
        model = ModelPodbor()
        view = ViewPodbor()
        ControlSelect.__init__(self, model, view)
    
    def acceptdialog(self):
        'Реагирование на согласие выбора'
        pprint(self.view.getcheckedrows())
    
    def modelischanged(self):
        'Реагирование на изменение модели'
        self.view.redraw(self.model.dataview) 