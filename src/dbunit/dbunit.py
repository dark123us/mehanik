#!/usr/bin/python
# -*- coding: UTF-8  -*-
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Time, Date, Interval, ForeignKey, DateTime, Float, Boolean
from sqlalchemy.orm import sessionmaker, relationship, backref
from sqlalchemy import create_engine
from datetime import date, timedelta, datetime


Base = declarative_base()

def SinglEngine():
    'Синглентон движка ORM'
    if not Engine._instance:
        Engine._instance = Engine()
    return Engine._instance

class Engine():
    'Движок ORM'
    _instance = None
    engine = None
    
    def setconnect(self, typedbasename, host, dbname, user, password, echo = False, charset = 'utf8'):
        'Подключение к ORM'
        self.engine = create_engine('%s://%s:%s@%s/%s?charset=%s'%
                                    (typedbasename, user, password, host, dbname,  charset)
                                    , echo = echo)
        
    def foo(self):
        return id(self)
        
    def getengine(self):
        'Возвращаем ссылку на подключение'
        return self.engine
    
    def createdatabase(self):
        'Удаляем таблицы и создаем заново'
        d = DataBase(self.engine)
        d.create_all()
        d.default()
    
    def getsession(self):
        'Получаем сессию для запросов'
        Session = sessionmaker(bind=self.engine)
        return Session()

#//////////////////////////////////////////////////////////////////////////////////////
class DataBase(object):
    'Управление базой данных'
    def __init__(self, engine):
        self.engine = engine
        Session = sessionmaker(bind=self.engine)
        self.session = Session()
        
    def create_all(self):
        'Удаляем и создаем все таблицы'
        Base.metadata.drop_all(self.engine)
        Base.metadata.create_all(self.engine)
    
    def default(self):
        'Устанавливаем начальные данные'
#        m1 = Marka(u'маз 113')
#        m2 = Marka(u'маз 105')
#        self.session.add(m1)
#        self.session.add(m2)
#        self.session.commit()
#        t = []
#        t += [ServiceTo1To2(m1.id,100,200)] 
#        t += [ServiceTo1To2(m2.id,150,250)]
#        for i in t:
#            self.session.add(i)
#        self.session.commit()
        self.session.add(TypeService(u'ТО-1', u'ТО-1', 1))
        self.session.add(TypeService(u'ТО-2', u'ТО-2', 2))
        self.session.add(TypeService(u'Тех.осмотр', u'Технический осмотр', 3))
        self.session.add(TypeService(u'Обяз.страх.', u'Обязательное страхование', 4))
        self.session.add(User(u'Пригодич', u'1'))
        self.session.commit()
        
#//////////////////////////////////////////////////////////////////////////////////////

class Marka(Base):
    'Марка'
    __tablename__ = 'marka'
    id          = Column(Integer, primary_key=True)
    name        = Column(String(200))
    crmid       = Column(Integer)
    crmidosnov  = Column(Integer)
    name2       = Column(String(200))
    
    def __init__(self, name=''):
        self.name = name
#//////////////////////////////////////////////////////////////////////////////////////
class Bus(Base):
    'Автобус'    
    __tablename__ = 'bus'
    id        = Column(Integer, primary_key=True)
    garnm     = Column(Integer)     #гаражный номер
    fullgarnm = Column(Integer)     #полный номер
    gosnm     = Column(String(20))  #госномер
    markaid   = Column(Integer, ForeignKey(Marka.id))     #ссылка на марку автобуса
    marka     = relationship(Marka)
    dtvvoda   = Column(Date)    #дата ввода
    dtvyvoda  = Column(Date)    #дата вывода
    
    def __init__(self, garnm = 0, gosnm = '', markaid = 0):
        self.garnm = garnm
        self.gosnm = gosnm
        self.markaid = markaid
#//////////////////////////////////////////////////////////////////////////////////////    
class TypeService(Base):
    __tablename__ = 'typeservice'
    id          = Column(Integer, primary_key=True)
    name        = Column(String(20))
    fullname    = Column(String(100))
    'Тип обслуживания'
    def __init__(self, name, fullname, id = None):
        if id: 
            self.id = id
        self.name = name
        self.fullname = fullname

#//////////////////////////////////////////////////////////////////////////////////////         
class Service(Base):
    'Обслуживание'
    __tablename__ = 'service'
    id              = Column(Integer, primary_key=True)
    busid           = Column(Integer, ForeignKey(Bus.id))     
    bus             = relationship(Bus)
    typeserviceid   = Column(Integer, ForeignKey(TypeService.id))
    typeservice     = relationship(TypeService) #тип обслуживания

#//////////////////////////////////////////////////////////////////////////////////////    
class ServiceTo1To2(Base):
    'справочник ТО-1 ТО-2'
    __tablename__ = 'serviceto1to2'
    id = Column(Integer, primary_key=True)
    markaid = Column(Integer, ForeignKey('marka.id'))     
    marka   = relationship(Marka, order_by = Marka.name)
    to1km   = Column(Integer)
    to2km   = Column(Integer)
    
    def __init__(self, markaid = 0, km1 = 0 , km2 = 0):
        self.markaid = markaid
        self.to1km = km1
        self.to2km = km2
    
    def __repr__(self):
        s = "[%d]%s\t%d\t%d"%(self.markaid, self.marka.name, self.to1km, self.to2km)
        return s
#//////////////////////////////////////////////////////////////////////////////////////
class Probeg(Base):
    __tablename__ = 'probeg'
    id      = Column(Integer, primary_key=True)
    bus     = relationship(Bus)
    busid   = Column(Integer, ForeignKey(Bus.id))
    dt      = Column(Date)
    km      = Column(Float)

    def __init__(self, busid = 0, dt = date.today(), km = 0.0):
        self.busid = busid
        self.dt = dt
        self.km = km
#//////////////////////////////////////////////////////////////////////////////////////
class User(Base):
    'Пользователи системы'
    __tablename__ = 'user'
    id         = Column(Integer, primary_key=True)
    name      = Column(String(50))
    password  = Column(String(50)) 
    
    def __init__(self, name = '', password = ''):
        self.name     = name
        self.password = password
    
#//////////////////////////////////////////////////////////////////////////////////////
class DocumentServiceTo1To2(Base):
    'Документ назначения дня прохождения ТО-1 ТО-2'
    __tablename__ = 'docserviceto1to2'
    docname    = u'Планирование ТО-1 ТО-2' 
    id         = Column(Integer, primary_key=True)
    docdate    = Column(DateTime)
    docnumber  = Column(String(10)) 
    userid     = Column(Integer, ForeignKey(User.id)) 
    author     = relationship(User)
    isheld     = Column(Boolean) 
    
    def __init__(self, datecreate = datetime.now(), 
                 number = '0', userid = 1):
        self.docdate    = datecreate
        self.docnumber  = number
        self.userid     = userid
        self.isheld     = False
#//////////////////////////////////////////////////////////////////////////////////////
class DocTableServiceTo1To2(Base):
    'Таблица документа назначения дня прохождения ТО-1 ТО-2'
    __tablename__ = 'doctableserviceto1to2'
    id              = Column(Integer, primary_key=True)
    docid           = Column(Integer, ForeignKey(DocumentServiceTo1To2.id))
    doc             = relationship(DocumentServiceTo1To2)
    busid           = Column(Integer, ForeignKey(Bus.id))     
    bus             = relationship(Bus)
    dt              = Column(Date)
    typeserviceid   = Column(Integer, ForeignKey(TypeService.id))
    typeservice     = relationship(TypeService) #тип обслуживания
    
    def __init__(self, docid, busid = 0, dt = None, tsid = 0):
        self.docid         = docid
        self.busid         = busid
        self.dt            = dt
        self.typeserviceid = 0
#//////////////////////////////////////////////////////////////////////////////////////
class DocumentReadyServiceTo1To2(Base):
    'Документ назначения дня прохождения ТО-1 ТО-2'
    __tablename__ = 'docreadyserviceto1to2'
    docname    = u'Прохождение ТО-1 ТО-2'
    id         = Column(Integer, primary_key=True)
    docdate    = Column(DateTime)
    docnumber  = Column(String(10)) 
    userid     = Column(Integer, ForeignKey(User.id)) 
    author     = relationship(User)
    isheld     = Column(Boolean) 
    
    def __init__(self, datecreate = datetime.now(), 
                 number = '0', userid = 1):
        self.docdate    = datecreate
        self.docnumber  = number
        self.userid     = userid
        self.isheld     = False
#//////////////////////////////////////////////////////////////////////////////////////
class DocTableReadyServiceTo1To2(Base):
    'Таблица документа назначения дня прохождения ТО-1 ТО-2'
    __tablename__ = 'doctablereadyserviceto1to2'
    id              = Column(Integer, primary_key=True)
    docid           = Column(Integer, ForeignKey(DocumentServiceTo1To2.id))
    doc             = relationship(DocumentServiceTo1To2)
    busid           = Column(Integer, ForeignKey(Bus.id))     
    bus             = relationship(Bus)
    dt              = Column(Date)
    km              = Column(Integer)
    typeserviceid   = Column(Integer, ForeignKey(TypeService.id))
    typeservice     = relationship(TypeService) #тип обслуживания
    
    def __init__(self, docid, busid = 0,  dt = None, km = 0, 
                 to1 = False, to2 = False):
        self.docid  = docid
        self.busid  = busid
        self.dt     = dt
        self.km     = km
        self.typeserviceid = 0

#//////////////////////////////////////////////////////////////////////////////////////
class VedomBusViezd(Base):
    'Ведомость контроля возвращения автобусов'
    __tablename__ = 'vedombusviezd'
    id      = Column(Integer, primary_key=True)
    busid   = Column(Integer, ForeignKey(Bus.id))     
    bus     = relationship(Bus)
    dt      = Column(DateTime)
    typedt  = Column(Integer)   #тип выезда 1 - выезд 1 смена план, 2 -выезд 1 смена факт 
    def __init__(self, busid, dt = datetime.now(), typedt = 1):
        self.busid  = busid
        self.dt     = dt
        self.typedt = typedt
