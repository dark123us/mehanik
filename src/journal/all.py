#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from datetime import datetime, date, time
from mvc.model import ModelJournal
from mvc.view import ViewJournal
from mvc.control import ControlJournal
from dbunit.dbunit import DocumentReadyServiceTo1To2, DocumentServiceTo1To2
from document.readyserviceto1to2 import ControlReadyServiceTo1To2
from document.serviceto1to2 import ControlServiceTo1To2

class ModelAllJournal(ModelJournal):
    
    def getlistclass(self):
        self.classlist = []
        self.classlist += [DocumentReadyServiceTo1To2]
        self.classlist += [DocumentServiceTo1To2]
        return self.classlist

class ViewAllJournal(ViewJournal):
    windowtitle = u'Общий журнал'
    
class ControlAllJournal(ControlJournal):
    ''
    def __init__(self, parent=None):
        self.parent = parent
        model = ModelAllJournal()
        view = ViewAllJournal()
        ControlJournal.__init__(self, model, view, parent=parent)
        view.setheader(model.header())
        model.readrecords()
        self.view.connect(self.view.table, QtCore.SIGNAL("cellDoubleClicked (int, int)"), self.dubleclicked)
        self.view.connect(self.view.table, QtCore.SIGNAL("pressed  (int, int)"), self.dubleclicked)
    
    def modelischanged(self):
        self.view.redraw(self.model.dataview)
    
    def dubleclicked(self, a, b):
        if type(self.model.data[a][1]) == DocumentReadyServiceTo1To2:
            doc = ControlReadyServiceTo1To2( self.model.data[a][1].id )
            self.parent.centralarea.addSubWindow(doc.widget());
            doc.show()
        elif type(self.model.data[a][1]) == DocumentServiceTo1To2:
            doc = ControlServiceTo1To2( self.model.data[a][1].id )
            self.parent.centralarea.addSubWindow(doc.widget());
            doc.show()
             
        